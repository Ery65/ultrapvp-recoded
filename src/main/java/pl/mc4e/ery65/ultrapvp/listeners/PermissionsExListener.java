package pl.mc4e.ery65.ultrapvp.listeners;

import java.util.UUID;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import pl.mc4e.ery65.ultrapvp.UltraPvp;
import ru.tehkode.permissions.events.PermissionEntityEvent;

public class PermissionsExListener implements Listener {
    
    @EventHandler
    void peev(PermissionEntityEvent e) {
        if (e.getEntity() == null || e.getEntityIdentifier() == null) return;
        UUID uuid = UUID.fromString(e.getEntityIdentifier());
        if (UltraPvp.getInstance().getPlayerManager().containsPlayer(uuid)) {
            UltraPvp.getInstance().getPlayerManager().getPlayer(uuid).setPermission(UltraPvp.getInstance().getPermissionManager()
                    .getHighestPermission(UltraPvp.getInstance().getServer().getPlayer(uuid), UltraPvp.getInstance().getPermission()));
        }
    }

}
