package pl.mc4e.ery65.ultrapvp.listeners;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import pl.mc4e.ery65.ultrapvp.UltraPvp;
import pl.mc4e.ery65.ultrapvp.configuration.Config;
import pl.mc4e.ery65.ultrapvp.utils.GamePlayer;

/**
 * Created by Ery65 on 2016-02-13.
 */
public class ChatListener implements Listener {

    @EventHandler
    void onChat(AsyncPlayerChatEvent e) {
        if (UltraPvp.getInstance().getPlayerManager().containsPlayer(e.getPlayer())) {
            if (UltraPvp.getInstance().getPlayerManager().getPlayer(e.getPlayer()).getLvl() >= Config.MIN_CHAT_LVL || e.getPlayer().hasPermission("ultrapvp.chat.bypass")) {
                if (Config.ENABLE_CHAT) {
                    if (Config.LOCKED_WORLDS.contains(e.getPlayer().getWorld().getName())) return;
                    GamePlayer player = UltraPvp.getInstance().getPlayerManager().getPlayer(e.getPlayer());
                    String additionalFormat = ChatColor.COLOR_CHAR + "8[" + Config.LVL_TITLE + " " + Config.LVL_COLOR + player.getLvl()
                            + ChatColor.COLOR_CHAR + "8] " + Config.RANG_COLOR + player.getGrade() + ChatColor.COLOR_CHAR + "r ";
                    e.setFormat(additionalFormat.replace("&", ChatColor.COLOR_CHAR + "") + e.getFormat());
                } else {
                    if (e.getFormat().contains(Config.LVL_NAME) || e.getFormat().contains(Config.GRADE_NAME)) {
                        GamePlayer player = UltraPvp.getInstance().getPlayerManager().getPlayer(e.getPlayer());
                        e.setFormat(e.getFormat().replace(Config.GRADE_NAME, player.getGrade()).replace(Config.LVL_NAME, player.getLvl() + ""));
                    }
                }
            } else {
                GamePlayer player = UltraPvp.getInstance().getPlayerManager().getPlayer(e.getPlayer());
                String message = UltraPvp.getInstance().getLangConfig().getMessage(player.getLang(), "chat-disable");
                e.getPlayer().sendMessage(message.replace("%lvl%", Config.MIN_CHAT_LVL+""));
                e.setCancelled(true);
            }
        }
    }
}
