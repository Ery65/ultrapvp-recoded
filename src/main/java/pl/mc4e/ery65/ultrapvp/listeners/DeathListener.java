package pl.mc4e.ery65.ultrapvp.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import pl.mc4e.ery65.ultrapvp.UltraPvp;
import pl.mc4e.ery65.ultrapvp.api.events.PlayerKillPlayerEvent;
import pl.mc4e.ery65.ultrapvp.configuration.Config;

import static pl.mc4e.ery65.ultrapvp.configuration.Config.DEBUG;

/**
 * Created by Ery65 on 2016-02-12.
 */
public class DeathListener implements Listener {

    @EventHandler
    void onDeath(PlayerDeathEvent e) {
        if (e.getEntity().getKiller() != null) {
            if (e.getEntity().getKiller() instanceof Player) {
                if (UltraPvp.getInstance().getPlayerManager().containsPlayer(e.getEntity()) &&
                        UltraPvp.getInstance().getPlayerManager().containsPlayer(e.getEntity().getKiller())) {
                    PlayerKillPlayerEvent evt = new PlayerKillPlayerEvent(e.getEntity().getKiller(), e.getEntity(),
                            UltraPvp.getInstance().getPlayerManager().getPlayer(e.getEntity().getKiller()),
                            UltraPvp.getInstance().getPlayerManager().getPlayer(e.getEntity()));
                    UltraPvp.getInstance().getServer().getPluginManager().callEvent(evt);
                }
            }
        }
    }

    @EventHandler
    void onKill(PlayerKillPlayerEvent e) {
        if (Config.LOCKED_WORLDS.contains(e.getKiller().getWorld().getName())) return;
        if (e.getKiller().equals(e.getKilled())) return;
        e.getKilledAsGamePlayer().addDeaths(1);
        e.getKillerAsGamePlayer().addKills(1);
        if (!e.getKillerAsGamePlayer().canKillAgain(e.getKilledAsGamePlayer().getUuid())) { if (DEBUG) System.out.println("Can\'t kill player! (Kill session limit)");return; }
        e.getKillerAsGamePlayer().addKilled(e.getKilledAsGamePlayer().getUuid());
        UltraPvp.getInstance().getPointsManager().addPoints(e.getKillerAsGamePlayer(), e.getKilledAsGamePlayer());
        if (e.getKillerAsGamePlayer().getLvl() == e.getKillerAsGamePlayer().getMaxLvl()) {
            if (DEBUG) System.out.println("Max lvl");
            if (Config.MAX_LVL_REWARDS) {
                if (UltraPvp.getInstance().getRewardManager().containsSpecialReward(e.getKillerAsGamePlayer()))
                    UltraPvp.getInstance().getRewardManager().getSpecialReward(e.getKillerAsGamePlayer())
                            .giveReward(e.getKiller(), e.getKillerAsGamePlayer(), e.getKilled().getName());
                if (UltraPvp.getInstance().getRewardManager().containsReward(e.getKillerAsGamePlayer()))
                    UltraPvp.getInstance().getRewardManager().getLvlReward(e.getKillerAsGamePlayer())
                            .giveReward(e.getKiller(), e.getKillerAsGamePlayer(), e.getKilled().getName());
            }
        } else {
            if (DEBUG) System.out.println("Adding lvl points.");
            e.getKillerAsGamePlayer().addLvlPoints(1);
            String msg = UltraPvp.getInstance().getLangConfig().getMessage(e.getKillerAsGamePlayer().getLang(), "player-gain-lvl");
            if (msg.length() > 1) {
                msg = msg.replace("%player%", e.getKilled().getName()).replace("%lvl%", e.getKillerAsGamePlayer().getLvl() + "");
                e.getKiller().sendMessage(msg);
            }
            if (UltraPvp.getInstance().getRewardManager().canIncreaseLvl(e.getKillerAsGamePlayer().getLvl())) {
                e.getKillerAsGamePlayer().addLvl(1);
                if (DEBUG) System.out.println("adding rewards");
                if (UltraPvp.getInstance().getRewardManager().containsSpecialReward(e.getKillerAsGamePlayer())) {
                    UltraPvp.getInstance().getRewardManager().getSpecialReward(e.getKillerAsGamePlayer())
                            .giveReward(e.getKiller(), e.getKillerAsGamePlayer(), e.getKilled().getName());
                    if (DEBUG) System.out.println("added special reward");
                }
                if (UltraPvp.getInstance().getRewardManager().containsReward(e.getKillerAsGamePlayer())) {
                    UltraPvp.getInstance().getRewardManager().getLvlReward(e.getKillerAsGamePlayer())
                            .giveReward(e.getKiller(), e.getKillerAsGamePlayer(), e.getKilled().getName());
                    if (DEBUG) System.out.println("added normal reward");
                }
                if (!UltraPvp.getInstance().getGradeManager().hasGoodGrade(e.getKillerAsGamePlayer())) {
                    if (DEBUG) System.out.println("Set good grade");
                    e.getKillerAsGamePlayer().setGrade(UltraPvp.getInstance().getGradeManager().getGrade(e.getKillerAsGamePlayer()));
                }
            }
        }
    }

    @EventHandler
    void onJoin(PlayerJoinEvent e) {
        if (!UltraPvp.getInstance().getPlayerManager().containsPlayer(e.getPlayer())) {
            if (DEBUG) System.out.println("Player joined");
            UltraPvp.getInstance().getPlayerManager().addPlayer(e.getPlayer());
        }
    }

    @EventHandler
    void onDisconnect(PlayerQuitEvent e) {
        if (UltraPvp.getInstance().getPlayerManager().containsPlayer(e.getPlayer())) {
            if (DEBUG) System.out.println("Player disconnected");
            UltraPvp.getInstance().getPlayerManager().removePlayer(e.getPlayer());
        }
    }

}
