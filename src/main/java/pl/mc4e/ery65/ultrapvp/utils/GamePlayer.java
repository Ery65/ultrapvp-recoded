package pl.mc4e.ery65.ultrapvp.utils;

import pl.mc4e.ery65.ultrapvp.UltraPvp;
import pl.mc4e.ery65.ultrapvp.configuration.Config;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class GamePlayer {

    private Integer lvl, kills, deaths, points, lastSpecialReward, lvlPoints, maxLvl, ksession;

    private UUID uuid;

    private String permission, grade, lang;

    private Map<UUID, Integer> howManyKills = new HashMap<UUID, Integer>();

    public GamePlayer(UUID player) {
        uuid = player;
        permission = "default";
        lvl = kills = deaths = lastSpecialReward = lvlPoints = ksession = 0;
        points = (Config.POINTS_TYPE.equalsIgnoreCase("elo")) ? Config.ELO_START_POINTS : 0;
        maxLvl = UltraPvp.getInstance().getRewardManager().getMaxLvl("default");
    }

    public GamePlayer(UUID player, String perm, String rang, String language, Integer lvl, Integer kills, Integer deaths, Integer points, Integer lastReward, Integer lvlPoint, Integer maxlevel) {
        uuid = player;
        permission = perm;
        this.grade = rang;
        this.lvl = lvl;
        this.kills = kills;
        this.deaths = deaths;
        this.points = points;
        lastSpecialReward = lastReward;
        lvlPoints = lvlPoint;
        maxLvl = maxlevel;
        ksession = 0;
        lang = language;
    }

    public GamePlayer(UUID player, String perm, String rang, Integer lvl, Integer kills, Integer deaths, Integer points, Integer lastReward, Integer lvlPoint, Integer maxlevel) {
        uuid = player;
        permission = perm;
        this.grade = rang;
        this.lvl = lvl;
        this.kills = kills;
        this.deaths = deaths;
        this.points = points;
        lastSpecialReward = lastReward;
        lvlPoints = lvlPoint;
        maxLvl = maxlevel;
        ksession = 0;
        lang = null;
    }

    public Integer getLvl() { return lvl; }

    public Integer getKills() { return kills; }

    public Integer getDeaths() { return deaths; }

    public Integer getPoints() { return points; }

    public Integer getLvlPoints() { return lvlPoints; }

    public String getPermission() { return permission; }

    public String getGrade() {return grade; }

    public Integer getLastSpecialReward() { return lastSpecialReward; }

    public UUID getUuid() { return uuid; }

    public void setLvl(Integer newlvl) { lvl = newlvl; }

    public void setKills(Integer newkills) { kills = newkills; }

    public void setDeaths(Integer newdeaths){ deaths = newdeaths; }

    public void setPoints(Integer newpoints) { points = newpoints; }

    public void setLvlPoints(Integer newpoints) { lvlPoints = newpoints; }

    public void setPermission(String perm) { permission = perm; }

    public void addDeaths(Integer toAdd) { deaths += toAdd; }

    public void addKills(Integer toAdd) { kills += toAdd; }

    public void addLvl(Integer toAdd) { lvl += toAdd; }

    public void decreaseLvl(Integer dec) { lvl -= dec; }

    public void addLvlPoints(Integer toAdd) { lvlPoints += toAdd; }

    public void decreaseLvlPoints(Integer dec) { lvlPoints -= dec; }

    public void addPoints(Integer toAdd) { points += toAdd; }

    public void decreasePoints(Integer dec) { points -= dec; }

    public void resetGamePlayer() { lvl = kills = deaths = lastSpecialReward = lvlPoints = 0; points = (Config.POINTS_TYPE.equalsIgnoreCase("elo")) ? Config.ELO_START_POINTS : 0; }

    public void setLastSpecialReward(Integer lastReward) { lastSpecialReward = lastReward; }

    public void setGrade(String grade) { this.grade = grade; }

    public String getLang() { return lang; }

    public void setLang(String language) { lang = language; }

    public Boolean canKillAgain(UUID uuid) {
        if (howManyKills.containsKey(uuid)) return howManyKills.get(uuid) < Config.MAX_SESSION;
        return Boolean.TRUE;
    }

    public Boolean canKillAgain(GamePlayer player) { return canKillAgain(player.getUuid()); }

    public void addKilled(UUID uuid) {
        if (howManyKills.containsKey(uuid)) {
            howManyKills.put(uuid, howManyKills.get(uuid) + 1);
        } else howManyKills.put(uuid, 1);
    }

    public void addKilled(GamePlayer player) { addKilled(player.getUuid()); }

    public void removeKiller(UUID uuid) {
        if (howManyKills.containsKey(uuid)) {
            howManyKills.remove(uuid);
        }
    }

    public void setMaxLvl(Integer maxLvl) {
        if (this.maxLvl == null) this.maxLvl = maxLvl;
        else if (this.maxLvl < maxLvl) {
            this.maxLvl = maxLvl;
        }
    }

    public Integer getMaxLvl() {
        return maxLvl;
    }

    public void increaseKillsSession() { ksession++; }

    public void removeKillsSession() {
        if (ksession > 0) ksession = 0;
        else ksession--;
    }

    public void resetLvl() {
        lvl = 0;
        lvlPoints = 0;
        points = (Config.POINTS_TYPE.equalsIgnoreCase("elo")) ? Config.ELO_START_POINTS : 0;
    }

    public void resetPoints() { points = (Config.POINTS_TYPE.equalsIgnoreCase("elo")) ? Config.ELO_START_POINTS : 0; }

    public Integer getKillsSession() { return ksession; }

    private Integer getValue() { return kills + deaths + maxLvl + lvl + lastSpecialReward + lvlPoints + points; }

    public Boolean compare(final GamePlayer player) { return getValue() >= player.getValue(); }

    public void loadFromPlayer(GamePlayer p) {
        kills = p.kills;
        deaths = p.deaths;
        lvl = p.lvl;
        maxLvl = p.maxLvl;
        lvlPoints = p.lvlPoints;
        lastSpecialReward = p.lastSpecialReward;
        points = p.points;
    }

}