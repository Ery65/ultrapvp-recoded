package pl.mc4e.ery65.ultrapvp.utils;

import org.bukkit.Bukkit;
import org.bukkit.command.*;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.plugin.*;
import pl.mc4e.ery65.simpleupdater.SimpleUpdater;
import pl.mc4e.ery65.ultrapvp.UltraPvp;
import pl.mc4e.ery65.ultrapvp.configuration.Config;

import java.io.*;
import java.lang.reflect.Field;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.*;

/**
 * Created by ery65 on 10.05.16.
 */
public class Updater {

    private static String version = null;

    private static String getWebVersion() {
        BufferedReader in = null;
        String privv = UltraPvp.getInstance().getDescription().getVersion();
        try {
            URL ulr = new URL("http://ery65.mc4e.pl/maven/public/pl/mc4e/ery65/ultrapvp/maven-metadata.xml");
            in = new BufferedReader(
                    new InputStreamReader(ulr.openStream()));

            String line;
            while ((line = in.readLine()) != null) {
                if (line.startsWith("      <version>")) {
                    privv = line.replace("      <version>", "").replace("</version>", "");
                }
            }
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return privv;
    }

    public static String getVersion() { return version; }

    public static Boolean isNewVersionAvailable() {
        String webv = Updater.getWebVersion();
        if (webv.toLowerCase().contains("snapshot") || webv.toLowerCase().contains("pre")) return false;
        String currentv = UltraPvp.getInstance().getDescription().getVersion();
        int current = 100 * Integer.parseInt(currentv.substring(0, 1));
        current += 9 * Integer.parseInt(currentv.substring(2, 3));
        current += 1 * Integer.parseInt(currentv.substring(4, 5));
        int web = 100 * Integer.parseInt(webv.substring(0, 1));
        web += 9 * Integer.parseInt(webv.substring(2, 3));
        web += 1 * Integer.parseInt(webv.substring(4, 5));
        Boolean ret = (current < web);
        if (ret) version = webv;
        return ret;
    }

    public static File download(String path, String filename, String version, String finalName) {
        if (version == null) return null;
        File file = null;
        try {
            URL url = new URL( ((path == null) ? "http://ery65.mc4e.pl/maven/public/pl/mc4e/ery65" : path) + "/" + filename
                    + "/" + version + "/" + filename + "-" + version + ".jar");
            ReadableByteChannel rbc = Channels.newChannel(url.openStream());
            file = new File("plugins" + File.separator + finalName + ".jar");
            FileOutputStream fos = new FileOutputStream(file);
            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }

    public static void downloadAndUpdate(final CommandSender cs) {
        final File plugin = download(null, "ultrapvp",  version, "ultrapvp-" + version);
        if (plugin == null) {
            if (cs instanceof ConsoleCommandSender) {
                cs.sendMessage(UltraPvp.getInstance().getLangConfig().getMessage(Config.LANGUAGE, "first-check"));
            } else {
                cs.sendMessage(UltraPvp.getInstance().getLangConfig().getMessage(UltraPvp.getInstance()
                        .getPlayerManager().getPlayer((Player)cs).getLang(), "first-check"));
            }
            return;
        }
        final File updater = download(null, "simpleupdater", "1.0.0", "SimpleUpdater");
        final Timer t = new Timer();
        t.schedule(new TimerTask() {
            @Override
            public void run() {
                enablePlugin(updater);
            }
        }, 5000);
        t.schedule(new TimerTask() {
            @Override
            public void run() {
                Plugin p = UltraPvp.getInstance().getServer().getPluginManager().getPlugin("SimpleUpdater");

                if (p == null || (!(p instanceof SimpleUpdater))) {
                    t.cancel();
                    return;
                }

                SimpleUpdater s = (SimpleUpdater)p;
                s.changeVersion(UltraPvp.getInstance(), plugin, cs);
                t.cancel();
            }
        }, 7000);
    }

    public static void enablePlugin(File newPlugin) {
        PluginManager man = Bukkit.getPluginManager();
        Plugin p = null;
        try {
            p = man.loadPlugin(newPlugin);
        } catch (InvalidPluginException e) {
            e.printStackTrace();
        } catch (InvalidDescriptionException e) {
            e.printStackTrace();
        }
        if (p != null) {
            man.enablePlugin(p);
        }
    }

    private static void unloadPlugin(Plugin p) {
        unload(p.getName());
    }

    /**
     * author @DonkeyTeeth2013
     * @param pluginName
     * @return
     */
    private static int unload(String pluginName) {
        pluginName = pluginName.toLowerCase().trim();
        PluginManager manager = Bukkit.getServer().getPluginManager();
        SimplePluginManager spm = (SimplePluginManager) manager;
        SimpleCommandMap commandMap = null;
        List<Plugin> plugins = null;
        Map<String, Plugin> lookupNames = null;
        Map<String, Command> knownCommands = null;
        Map<Event, SortedSet<RegisteredListener>> listeners = null;
        boolean reloadlisteners = true;
        try {
            if (spm != null) {
                Field pluginsField = spm.getClass().getDeclaredField("plugins");
                pluginsField.setAccessible(true);
                plugins = (List<Plugin>) pluginsField.get(spm);

                Field lookupNamesField = spm.getClass().getDeclaredField("lookupNames");
                lookupNamesField.setAccessible(true);
                lookupNames = (Map<String, Plugin>) lookupNamesField.get(spm);

                try {
                    Field listenersField = spm.getClass().getDeclaredField("listeners");
                    listenersField.setAccessible(true);
                    listeners = (Map<Event, SortedSet<RegisteredListener>>) listenersField.get(spm);
                } catch (Exception e) {
                    reloadlisteners = false;
                }

                Field commandMapField = spm.getClass().getDeclaredField("commandMap");
                commandMapField.setAccessible(true);
                commandMap = (SimpleCommandMap) commandMapField.get(spm);

                Field knownCommandsField = commandMap.getClass().getDeclaredField("knownCommands");
                knownCommandsField.setAccessible(true);
                knownCommands = (Map<String, Command>) knownCommandsField.get(commandMap);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        boolean in = false;

        for (Plugin pl : Bukkit.getServer().getPluginManager().getPlugins()) {
            if (in)
                break;
            if (pl.getName().toLowerCase().startsWith(pluginName.toLowerCase())) {
                manager.disablePlugin(pl);
                if (plugins != null && plugins.contains(pl))
                    plugins.remove(pl);

                if (lookupNames != null && lookupNames.containsKey(pl.getName())) {
                    lookupNames.remove(pl.getName());
                }

                if (listeners != null && reloadlisteners) {
                    for (SortedSet<RegisteredListener> set : listeners.values()) {
                        for (Iterator<RegisteredListener> it = set.iterator(); it.hasNext();) {
                            RegisteredListener value = it.next();

                            if (value.getPlugin() == pl) {
                                it.remove();
                            }
                        }
                    }
                }

                if (commandMap != null) {
                    for (Iterator<Map.Entry<String, Command>> it = knownCommands.entrySet().iterator(); it.hasNext();) {
                        Map.Entry<String, Command> entry = it.next();
                        if (entry.getValue() instanceof PluginCommand) {
                            PluginCommand c = (PluginCommand) entry.getValue();
                            if (c.getPlugin() == pl) {
                                c.unregister(commandMap);
                                it.remove();
                            }
                        }
                    }
                }
                for (Plugin plu : Bukkit.getServer().getPluginManager().getPlugins()) {
                    if (plu.getDescription().getDepend() != null) {
                        for (String depend : plu.getDescription().getDepend()) {
                            if (depend.equalsIgnoreCase(pl.getName())) {
                                System.out.println("[Unloading Plugin] " + plu.getName() + " must be disabled!");
                                unload(plu.getName());
                                return 1; //dependencies also disabled
                            }
                        }
                    }
                }
                in = true;
            }
        }
        if (!in) {
            System.out.println("Not an existing plugin");
            return -1; //non-existent
        }
        System.gc();
        return 0; //success
    }

}
