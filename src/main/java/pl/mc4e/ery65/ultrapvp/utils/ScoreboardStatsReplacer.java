package pl.mc4e.ery65.ultrapvp.utils;

import com.github.games647.scoreboardstats.variables.ReplaceEvent;
import com.github.games647.scoreboardstats.variables.VariableReplaceAdapter;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import pl.mc4e.ery65.ultrapvp.api.UltraPvpAPI;
import pl.mc4e.ery65.ultrapvp.utils.GamePlayer;

/**
 * Created by Ery65 on 2016-08-15.
 */
public class ScoreboardStatsReplacer extends VariableReplaceAdapter<Plugin> {

    private UltraPvpAPI api;

    public ScoreboardStatsReplacer(Plugin plugin, UltraPvpAPI api, String... variables) {
        super(plugin, variables);
        this.api = api;
    }

    @Override
    public void onReplace(Player player, String variable, ReplaceEvent replaceEvent) {
        if (api.containsPlayer(player)) {
            GamePlayer p = api.getPlayer(player);
            if ("ultrapvp_kills".equals(variable)) {
                replaceEvent.setScore(p.getKills());
                return;
            }

            if ("ultrapvp_deaths".equals(variable)) {
                replaceEvent.setScore(p.getDeaths());
                return;
            }

            if ("ultrapvp_ks".equals(variable)) {
                replaceEvent.setScore(p.getKillsSession());
                return;
            }

            if ("ultrapvp_lvl".equals(variable)) {
                replaceEvent.setScore(p.getLvl());
                return;
            }

            if ("ultrapvp_points".equals(variable)) {
                replaceEvent.setScore(p.getPoints());
                return;
            }

            if ("ultrapvp_lvlp".equals(variable)) {
                replaceEvent.setScore(p.getLvlPoints());
                return;
            }

            if ("ultrapvp_maxlvl".equals(variable)) {
                replaceEvent.setScore(p.getMaxLvl());
                return;
            }

            if ("ultrapvp_lsr".equals(variable)) {
                replaceEvent.setScore(p.getLastSpecialReward());
                return;
            }
        }
    }
}
