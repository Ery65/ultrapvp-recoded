package pl.mc4e.ery65.ultrapvp.utils;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import pl.mc4e.ery65.ultrapvp.UltraPvp;
import pl.mc4e.ery65.ultrapvp.api.events.PlayerGainRewardEvent;

import java.util.List;

/**
 * Created by Ery65 on 2016-02-11.
 */
public class SimpleReward {

    private ItemStack[] items = null;

    private Double money = null;

    private String[] commands = null;

    public SimpleReward(ItemStack[] items, List<String> commands, Double money) {
        this.items = items;
        if (commands != null) {
            this.commands = new String[commands.size()];
            for (Integer i = 0; i < commands.size(); i++) {
                this.commands[i] = commands.get(i);
            }
        }
        this.money = money;
    }

    public Boolean hasMoney() { return money != null; }

    public Boolean hasCommands() { return commands != null; }

    public Boolean hasItems() { return items != null; }

    public ItemStack[] getItems() {
        return items;
    }

    public Double getMoney() {
        return money;
    }

    public String[] getCommands() {
        return commands;
    }

    public void giveReward(final Player player, final GamePlayer gplayer, final String killed, final Double moneyAddon, final Integer itemAddon) {
        PlayerGainRewardEvent evt = new PlayerGainRewardEvent(this, UltraPvp.getInstance().getRewardManager().isSpecialReward(this), gplayer);
        UltraPvp.getInstance().getServer().getPluginManager().callEvent(evt);
        if (evt.isCancelled()) {
            return;
        }
        UltraPvp.getInstance().getServer().getScheduler().runTaskAsynchronously(UltraPvp.getInstance(), new Runnable() {

            @Override
            public void run() {
                if (hasMoney() && UltraPvp.getInstance().hasEconomy()) {
                    UltraPvp.getInstance().getEconomy().depositPlayer(player, getMoney() + moneyAddon);
                    String message = UltraPvp.getInstance().getLangConfig().getMessage(gplayer.getLang(), "player-gain-money");
                    if (message.length() > 1) {
                        message = message.replace("%player%", killed).replace("%money%", getMoney() + moneyAddon + "")
                                .replace("%lvl%", gplayer.getLvl() + "");
                        player.sendMessage(message);
                    }
                }
                if (hasItems()) {
                    if (player.getInventory().firstEmpty() != -1) {
                        for (ItemStack it : getItems()) {
                            if (it == null) continue;
                            ItemStack item = it.clone();
                            item.setAmount(item.getAmount() + itemAddon);
                            player.getInventory().addItem(item);
                            String message = UltraPvp.getInstance().getLangConfig().getMessage(gplayer.getLang(), "player-gain-reward");
                            if (message.length() > 1) {
                                message = message.replace("%player%", killed).replace("%amount%", item.getAmount() + "")
                                        .replace("%item%", item.getType().name()).replace("%lvl%", gplayer.getLvl() + "");
                                player.sendMessage(message);
                            }
                        }
                    } else {
                        String message = UltraPvp.getInstance().getLangConfig().getMessage(gplayer.getLang(), "no-slot-message");
                        if (message.length() > 1) {
                            player.sendMessage(message);
                        }
                    }
                }
                if (hasCommands()) {
                    for (String cmd : getCommands()) {
                        if (cmd == null) continue;
                        String newcmd = cmd.replace("%player%", player.getName()).replace("%lvl%", gplayer.getLvl()+ "")
                                .replace("%money%", getMoney() + moneyAddon + "");
                        UltraPvp.getInstance().getServer().dispatchCommand(UltraPvp.getInstance().getServer().getConsoleSender(), newcmd);
                    }
                }
            }

        });
    }

    public void giveReward(final Integer lvl, final Player player, final GamePlayer p) {
        PlayerGainRewardEvent evt = new PlayerGainRewardEvent(this, UltraPvp.getInstance().getRewardManager().isSpecialReward(this), p);
        UltraPvp.getInstance().getServer().getPluginManager().callEvent(evt);
        if (evt.isCancelled()) {
            return;
        }
        UltraPvp.getInstance().getServer().getScheduler().runTaskAsynchronously(UltraPvp.getInstance(), new Runnable() {

            @Override
            public void run() {
                if (hasMoney() && UltraPvp.getInstance().hasEconomy()) {
                    UltraPvp.getInstance().getEconomy().depositPlayer(player, getMoney());
                    String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "player-gain-money");
                    if (message.length() > 1) {
                        message = message.replace("%player%", "").replace("%money%", getMoney()+"")
                                .replace("%lvl%", lvl + "");
                        player.sendMessage(message);
                    }
                }
                if (hasItems()) {
                    if (player.getInventory().firstEmpty() != -1) {
                        for (ItemStack it : getItems()) {
                            if (it == null) continue;
                            ItemStack item = it.clone();
                            item.setAmount(item.getAmount());
                            player.getInventory().addItem(item);
                            String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "player-gain-reward");
                            if (message.length() > 1) {
                                message = message.replace("%player%", "").replace("%amount%", item.getAmount() + "")
                                        .replace("%item%", item.getType().name()).replace("%lvl%", lvl + "");
                                player.sendMessage(message);
                            }
                        }
                    } else {
                        String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "no-slot-message");
                        if (message.length() > 1) {
                            player.sendMessage(message);
                        }
                    }
                }
                if (hasCommands()) {
                    for (String cmd : getCommands()) {
                        if (cmd == null) continue;
                        String newcmd = cmd.replace("%player%", player.getName()).replace("%lvl%", lvl+ "")
                                .replace("%money%", getMoney() + "");
                        UltraPvp.getInstance().getServer().dispatchCommand(UltraPvp.getInstance().getServer().getConsoleSender(), newcmd);
                    }
                }
            }

        });
    }

    public void giveReward(final Player player, final GamePlayer gplayer, final String killed, final Double addon) {
        giveReward(player, gplayer, killed, addon, (int) addon.doubleValue());
    }

    public void giveReward(final Player player, final GamePlayer gplayer, final String killed) { giveReward(player, gplayer, killed, 0.0, 0); }

}
