package pl.mc4e.ery65.ultrapvp.commands;

import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import pl.mc4e.ery65.ultrapvp.UltraPvp;
import pl.mc4e.ery65.ultrapvp.configuration.Config;
import pl.mc4e.ery65.ultrapvp.utils.GamePlayer;
import pl.mc4e.ery65.ultrapvp.utils.Updater;

/**
 * Created by ery65 on 16.04.16.
 */
public class AdminCommands implements CommandExecutor {

    @Override
    public boolean onCommand(final CommandSender cs, final Command cmd, final String label, final String[] args) {
        Boolean console = false;
        if (cs instanceof ConsoleCommandSender) console = true;
        if (!console && !cs.hasPermission("ultrapvp.admin")) {
            cs.sendMessage(UltraPvp.getInstance().getLangConfig().getMessage(UltraPvp.getInstance().getPlayerManager().
                    getPlayer((Player)cs).getLang(), "no-permission"));
            return true;
        }
        if (args.length == 0) {
            return getVersion(console, cs);
        } else if (args.length == 1) {
            if (args[0].equalsIgnoreCase("version")) return getVersion(console, cs);
            else if (args[0].equalsIgnoreCase("reload")) return reloadCommand(console, cs);
            else if (args[0].equalsIgnoreCase("player")) return false;
            else if (args[0].equalsIgnoreCase("help")) return helpCommand(console, cs, 0);
            else if (args[0].equalsIgnoreCase("update")) {
                if (console) {
                    cs.sendMessage(UltraPvp.getInstance().getLangConfig().getMessage(Config.LANGUAGE, "update-in-progress"));
                } else {
                    cs.sendMessage(UltraPvp.getInstance().getLangConfig().getMessage(UltraPvp.getInstance()
                            .getPlayerManager().getPlayer((Player)cs).getLang(), "update-in-progress"));
                }
                Updater.downloadAndUpdate(cs);
                return true;
            } else if (args[0].equalsIgnoreCase("check")) {
                final boolean c = console;
                UltraPvp.getInstance().getServer().getScheduler().runTaskAsynchronously(UltraPvp.getInstance(), new Runnable() {
                    @Override
                    public void run() {
                        if (Updater.isNewVersionAvailable()) newVersionAvailable(c, cs, Updater.getVersion());
                        else alreadyUpToDate(c, cs);
                    }
                });
                return true;
            }
        } else if (args.length == 2) {
            if (args[0].equalsIgnoreCase("save") && args[1].equalsIgnoreCase("all")) {
                return saveAllCommand(console, cs);
            } else if (args[0].equalsIgnoreCase("help")) {
                try {
                    int i = Integer.parseInt(args[1]);
                    return helpCommand(console, cs, i);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    wrongNumber(console, cs);
                    return true;
                }
            } else return false;
        } else if (args.length == 3) {
            if (args[0].equalsIgnoreCase("player")) {
                final OfflinePlayer p = UltraPvp.getInstance().getServer().getOfflinePlayer(args[1]);
                if (p == null || !p.hasPlayedBefore()) {
                    return wrongPlayer(console, cs);
                }
                if (p.isOnline()) {
                    if (args[2].equalsIgnoreCase("reload")) {
                        final boolean c = console;
                        UltraPvp.getInstance().getServer().getScheduler().runTaskAsynchronously(UltraPvp.getInstance(), new Runnable() {
                            @Override
                            public void run() {
                                UltraPvp.getInstance().getDatabaseManager().loadPlayer(UltraPvp.getInstance().getPlayerManager().getPlayer((Player)p));
                                querySuccesfull(c, cs);
                            }
                        });
                        return true;
                    } else  if (args[2].equalsIgnoreCase("remove")) {
                        if (console)
                            cs.sendMessage(UltraPvp.getInstance().getLangConfig().getMessage(Config.LANGUAGE, "cant-when-online"));
                        else
                            cs.sendMessage(UltraPvp.getInstance().getLangConfig().getMessage(UltraPvp.getInstance()
                                    .getPlayerManager().getPlayer((Player)cs).getLang(), "cant-when-online"));
                        return true;
                    } else if (args[2].equalsIgnoreCase("reset")) {
                        UltraPvp.getInstance().getPlayerManager().getPlayer((Player)p).resetGamePlayer();
                        return querySuccesfull(console, cs);
                    } else if (args[2].equalsIgnoreCase("save")) {
                        UltraPvp.getInstance().getDatabaseManager().savePlayer(UltraPvp.getInstance().getPlayerManager().getPlayer(p.getUniqueId()));
                        return querySuccesfull(console, cs);
                    }
                } else {
                    if (args[2].equalsIgnoreCase("remove")) {
                        UltraPvp.getInstance().getDatabaseManager().executeQuery("DELETE FROM `ultrapvp_players` WHERE `uuid`='"
                                + p.getUniqueId().toString() + "\'");
                        return querySuccesfull(console, cs);
                    } else if (args[2].equalsIgnoreCase("reset")) {
                        GamePlayer resetted = new GamePlayer(p.getUniqueId());
                        UltraPvp.getInstance().getDatabaseManager().savePlayer(resetted);
                        return querySuccesfull(console, cs);
                    }
                }

            } else return false;
        } else if (args.length == 4) {
            if (!args[0].equalsIgnoreCase("player") && !args[2].equalsIgnoreCase("points") && args[3].equalsIgnoreCase("reset")) return false;
            OfflinePlayer p = UltraPvp.getInstance().getServer().getOfflinePlayer(args[0]);
            if (p == null || !p.hasPlayedBefore()) return wrongPlayer(console, cs);
            if (p.isOnline()) {
                UltraPvp.getInstance().getPlayerManager().getPlayer(p.getUniqueId()).resetPoints();
            }
            UltraPvp.getInstance().getDatabaseManager().executeQuery("UPDATE `ultrapvp_players` SET `points`=" +
                    ((Config.POINTS_TYPE.equalsIgnoreCase("elo")) ? Config.ELO_START_POINTS : 0)
                    + " WHERE `uuid`='" + p.getUniqueId().toString() + "\'");
            return querySuccesfull(console, cs);
        } else if (args.length == 5) {
            if (!args[0].equalsIgnoreCase("player") && !args[2].equalsIgnoreCase("points")) return false;
            OfflinePlayer p = UltraPvp.getInstance().getServer().getOfflinePlayer(args[1]);
            int i;
            try {
                i = Integer.parseInt(args[4]);
            } catch (NumberFormatException e) {
                e.printStackTrace();
                wrongNumber(console, cs);
                return true;
            }
            if (p == null || !p.hasPlayedBefore()) {
                return wrongPlayer(console, cs);
            }
            if (p.isOnline()) {
                if (args[3].equalsIgnoreCase("add")) {
                    UltraPvp.getInstance().getPlayerManager().getPlayer((Player) p).addPoints(i);
                    return querySuccesfull(console, cs);
                } else if (args[3].equalsIgnoreCase("remove")) {
                    UltraPvp.getInstance().getPlayerManager().getPlayer((Player) p).decreasePoints(i);
                    return querySuccesfull(console, cs);
                } else if (args[3].equalsIgnoreCase("set")) {
                    UltraPvp.getInstance().getPlayerManager().getPlayer((Player) p).setPoints(i);
                    return querySuccesfull(console, cs);
                } else return false;
            } else {
                if (args[3].equalsIgnoreCase("add")) {
                    UltraPvp.getInstance().getDatabaseManager().executeQuery("UPDATE `ultrapvp_players` SET `points`=`points`+"
                            + i + " WHERE `uuid`=\'" + p.getUniqueId().toString() + "\'");
                    return querySuccesfull(console, cs);
                } else if (args[3].equalsIgnoreCase("remove")) {
                    UltraPvp.getInstance().getDatabaseManager().executeQuery("UPDATE `ultrapvp_players` SET `points`=`points`-"
                            + i + " WHERE `uuid`=\'" + p.getUniqueId().toString() + "\'");
                    return querySuccesfull(console, cs);
                } else if (args[3].equalsIgnoreCase("set")) {
                    UltraPvp.getInstance().getDatabaseManager().executeQuery("UPDATE `ultrapvp_players` SET `points`="
                            + i + " WHERE `uuid`=\'" + p.getUniqueId().toString() + "\'");
                    return querySuccesfull(console, cs);
                } else return false;
            }
        }
        return true;
    }

    private Boolean getVersion(Boolean console, CommandSender cs) {
        if (console)
            cs.sendMessage(UltraPvp.getInstance().getLangConfig().getMessage(Config.LANGUAGE, "plugin-version").replace("%version%", UltraPvp.getInstance().getDescription().getVersion()));
        else
            cs.sendMessage(UltraPvp.getInstance().getLangConfig().getMessage(UltraPvp.getInstance().getPlayerManager().
                    getPlayer((Player)cs).getLang(), "plugin-version").replace("%version%", UltraPvp.getInstance().getDescription().getVersion()));
        return true;
    }

    private Boolean reloadCommand(final Boolean console, final CommandSender cs) {
        UltraPvp.getInstance().getServer().getScheduler().runTaskAsynchronously(UltraPvp.getInstance(), new Runnable() {
            @Override
            public void run() {
                Config.reload();
                UltraPvp.getInstance().getLangConfig().reload();
                if (console)
                    cs.sendMessage(UltraPvp.getInstance().getLangConfig().getMessage(Config.LANGUAGE, "reload"));
                else
                    cs.sendMessage(UltraPvp.getInstance().getLangConfig().getMessage(UltraPvp.getInstance().getPlayerManager().getPlayer((Player)cs).getLang(), "reload"));
            }
        });
        return true;
    }

    private Boolean helpCommand(Boolean console, CommandSender cs, Integer page) {
        if (console)
            cs.sendMessage(UltraPvp.getInstance().getLangConfig().getHelp(Config.LANGUAGE, page, console));
        else
            cs.sendMessage(UltraPvp.getInstance().getLangConfig().getHelp(UltraPvp.getInstance()
                    .getPlayerManager().getPlayer((Player)cs).getLang(), page, console));
        return true;
    }

    private Boolean saveAllCommand(final Boolean console, final CommandSender cs) {
        UltraPvp.getInstance().getServer().getScheduler().runTaskAsynchronously(UltraPvp.getInstance(), new Runnable() {
            @Override
            public void run() {
                UltraPvp.getInstance().getPlayerManager().saveAll();
                if (console) {
                    cs.sendMessage(UltraPvp.getInstance().getLangConfig().getMessage(Config.LANGUAGE, "save-all"));
                } else {
                    cs.sendMessage(UltraPvp.getInstance().getLangConfig().getMessage(UltraPvp.getInstance()
                            .getPlayerManager().getPlayer((Player)cs).getLang(), "save-all"));
                }
            }
        });
        return true;
    }

    private Boolean querySuccesfull(Boolean console, CommandSender cs) {
        if (console)
            cs.sendMessage(UltraPvp.getInstance().getLangConfig().getMessage(Config.LANGUAGE, "query-succesfull"));
        else
            cs.sendMessage(UltraPvp.getInstance().getLangConfig().getMessage(UltraPvp.getInstance()
                    .getPlayerManager().getPlayer((Player)cs).getLang(), "query-succesfull"));
        return true;
    }

    private Boolean wrongPlayer(Boolean console, CommandSender cs) {
        if (console)
            cs.sendMessage(UltraPvp.getInstance().getLangConfig().getMessage(Config.LANGUAGE, "wrong-player"));
        else
            cs.sendMessage(UltraPvp.getInstance().getLangConfig().getMessage(UltraPvp.getInstance()
                    .getPlayerManager().getPlayer((Player)cs).getLang(), "wrong-player"));
        return true;
    }

    private Boolean newVersionAvailable(Boolean console, CommandSender cs, String version) {
        if (console)
            cs.sendMessage(UltraPvp.getInstance().getLangConfig().getMessage(Config.LANGUAGE, "new-version-available").replace("%version%", version));
        else
            cs.sendMessage(UltraPvp.getInstance().getLangConfig().getMessage(UltraPvp.getInstance()
                    .getPlayerManager().getPlayer((Player)cs).getLang(), "new-version-available").replace("%version%", version));
        return true;
    }

    private Boolean alreadyUpToDate(Boolean console, CommandSender cs) {
        if (console)
            cs.sendMessage(UltraPvp.getInstance().getLangConfig().getMessage(Config.LANGUAGE, "good-version"));
        else
            cs.sendMessage(UltraPvp.getInstance().getLangConfig().getMessage(UltraPvp.getInstance()
                    .getPlayerManager().getPlayer((Player)cs).getLang(), "good-version"));
        return true;
    }

    private void wrongNumber(Boolean console, CommandSender cs) {
        if (console)
            cs.sendMessage(UltraPvp.getInstance().getLangConfig().getMessage(Config.LANGUAGE, "bad-value"));
        else
            cs.sendMessage(UltraPvp.getInstance().getLangConfig().getMessage(UltraPvp.getInstance()
                    .getPlayerManager().getPlayer((Player)cs).getLang(), "bad-value"));
    }

}
