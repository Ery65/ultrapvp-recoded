package pl.mc4e.ery65.ultrapvp.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import pl.mc4e.ery65.ultrapvp.UltraPvp;
import pl.mc4e.ery65.ultrapvp.configuration.Config;
import pl.mc4e.ery65.ultrapvp.utils.GamePlayer;

/**
 * Created by Ery65 on 2016-02-24.
 */
public class LvlCommands implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
        if (!(cs instanceof Player)) {
            String message = UltraPvp.getInstance().getLangConfig().getMessage(null, "lvls-console");
            cs.sendMessage(message);
            return true;
        }
        GamePlayer p = UltraPvp.getInstance().getPlayerManager().getPlayer((Player) cs);
        if (args.length > 3) {
            String message = UltraPvp.getInstance().getLangConfig().getMessage(null, "lvl-usage");
            cs.sendMessage(message);
        } else if (args.length == 0) {
            showLvl(null, cs, p);
        } else if (args.length == 1) {
            if (args[0].equalsIgnoreCase("reset")) {
                if (!cs.hasPermission("ultrapvp.lvl.reset")) {
                    String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "no-permission");
                    cs.sendMessage(message);
                    return true;
                }
                reset((Player) cs, p);
            } else {
                if (!cs.hasPermission("ultrapvp.lvl.other")) {
                    String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "no-permission");
                    cs.sendMessage(message);
                    return true;
                }
                showLvl(args[0], cs, p);
            }
        } else if (args.length == 2) {
            if (args[1].equalsIgnoreCase("reset")) {
                if (!cs.hasPermission("ultrapvp.lvl.reset")) {
                    String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "no-permission");
                    cs.sendMessage(message);
                    return true;
                }
                if (!cs.hasPermission("ultrapvp.lvl.reset.other")) {
                    String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "no-permission");
                    cs.sendMessage(message);
                    return true;
                }
                GamePlayer pl = UltraPvp.getInstance().getAPI().getPlayer(args[0]);
                if (pl == null) {
                    String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "wrong-player");
                    cs.sendMessage(message.replace("%player%", args[0]));
                    return true;
                }
                reset((Player) cs, pl);
            } else if (args[0].equalsIgnoreCase("reset")) {
                if (!cs.hasPermission("ultrapvp.lvl.reset")) {
                    String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "no-permission");
                    cs.sendMessage(message);
                    return true;
                }
                if (!cs.hasPermission("ultrapvp.lvl.reset.other")) {
                    String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "no-permission");
                    cs.sendMessage(message);
                    return true;
                }
                GamePlayer pl = UltraPvp.getInstance().getAPI().getPlayer(args[1]);
                if (pl == null) {
                    String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "wrong-player");
                    cs.sendMessage(message.replace("%player%", args[1]));
                    return true;
                }
                reset((Player) cs, pl);
            } else {
                String message = UltraPvp.getInstance().getLangConfig().getMessage(null, "lvl-usage");
                cs.sendMessage(message);
                return true;
            }
        } else if (args.length == 3) {
            if (args[1].equalsIgnoreCase("add") || args[1].equalsIgnoreCase("dodaj")) {
                if (!cs.hasPermission("ultrapvp.lvl.add")) {
                    String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "no-permission");
                    cs.sendMessage(message);
                    return true;
                }
                Integer howMany = 0;
                try {
                    howMany = Integer.parseInt(args[2]);
                } catch (NumberFormatException e) {
                    String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "bad-value");
                    cs.sendMessage(message.replace("%value%", args[2]));
                    return true;
                }
                Player player = UltraPvp.getInstance().getServer().getPlayer(args[0]);
                if (player == null) {
                    String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "wrong-player");
                    cs.sendMessage(message.replace("%player%", args[0]));
                    return true;
                }
                if (cs.equals(player)) {
                    if (howMany < 0) {
                        removeLvl(p, cs, null, -howMany, null);
                    } else {
                        addLvl(p, cs, null, howMany, true, null);
                    }
                } else {
                    if (howMany < 0) {
                        removeLvl(p, cs, UltraPvp.getInstance().getPlayerManager().getPlayer(player), -howMany, player.getName());
                    } else {
                        addLvl(p, cs, UltraPvp.getInstance().getPlayerManager().getPlayer(player), howMany, true, player.getName());
                    }
                }
            } else if (args[0].equalsIgnoreCase("add") || args[0].equalsIgnoreCase("dodaj")) {
                if (!cs.hasPermission("ultrapvp.lvl.add")) {
                    String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "no-permission");
                    cs.sendMessage(message);
                    return true;
                }
                Integer howMany = 0;
                try {
                    howMany = Integer.parseInt(args[2]);
                } catch (NumberFormatException e) {
                    String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "bad-value");
                    cs.sendMessage(message.replace("%value%", args[2]));
                    return true;
                }
                Player player = UltraPvp.getInstance().getServer().getPlayer(args[1]);
                if (player == null) {
                    String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "wrong-player");
                    cs.sendMessage(message.replace("%player%", args[1]));
                    return true;
                }
                if (cs.equals(player)) {
                    if (howMany < 0) {
                        removeLvl(p, cs, null, -howMany, null);
                    } else {
                        addLvl(p, cs, null, howMany, true, null);
                    }
                } else {
                    if (howMany < 0) {
                        removeLvl(p, cs, UltraPvp.getInstance().getPlayerManager().getPlayer(player), -howMany, player.getName());
                    } else {
                        addLvl(p, cs, UltraPvp.getInstance().getPlayerManager().getPlayer(player), howMany, true, player.getName());
                    }
                }
            } else if (args[1].equalsIgnoreCase("remove") || args[1].equalsIgnoreCase("odejmij")) {
                if (!cs.hasPermission("ultrapvp.lvl.remove")) {
                    String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "no-permission");
                    cs.sendMessage(message);
                    return true;
                }
                Integer howMany = 0;
                try {
                    howMany = Integer.parseInt(args[2]);
                } catch (NumberFormatException e) {
                    String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "bad-value");
                    cs.sendMessage(message.replace("%value%", args[2]));
                    return true;
                }
                Player player = UltraPvp.getInstance().getServer().getPlayer(args[0]);
                if (player == null) {
                    String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "wrong-player");
                    cs.sendMessage(message.replace("%player%", args[0]));
                    return true;
                }
                if (cs.equals(player)) {
                    if (howMany < 0) {
                        addLvl(p, cs, null, -howMany, true, null);
                    } else {
                        removeLvl(p, cs, null, howMany, null);
                    }
                } else {
                    if (howMany < 0) {
                        addLvl(p, cs, UltraPvp.getInstance().getPlayerManager().getPlayer(player), -howMany, true, player.getName());
                    } else {
                        removeLvl(p, cs, UltraPvp.getInstance().getPlayerManager().getPlayer(player), howMany, player.getName());
                    }
                }
            } else if (args[0].equalsIgnoreCase("remove") || args[0].equalsIgnoreCase("odejmij")) {
                if (!cs.hasPermission("ultrapvp.lvl.remove")) {
                    String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "no-permission");
                    cs.sendMessage(message);
                    return true;
                }
                Integer howMany = 0;
                try {
                    howMany = Integer.parseInt(args[2]);
                } catch (NumberFormatException e) {
                    String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "bad-value");
                    cs.sendMessage(message.replace("%value%", args[2]));
                    return true;
                }
                Player player = UltraPvp.getInstance().getServer().getPlayer(args[1]);
                if (player == null) {
                    String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "wrong-player");
                    cs.sendMessage(message.replace("%player%", args[1]));
                    return true;
                }
                if (cs.equals(player)) {
                    if (howMany < 0) {
                        addLvl(p, cs, null, -howMany, true, null);
                    } else {
                        removeLvl(p, cs, null, howMany, null);
                    }
                } else {
                    if (howMany < 0) {
                        addLvl(p, cs, UltraPvp.getInstance().getPlayerManager().getPlayer(player), -howMany, true, player.getName());
                    } else {
                        removeLvl(p, cs, UltraPvp.getInstance().getPlayerManager().getPlayer(player), howMany, player.getName());
                    }
                }
            } else if (args[1].equalsIgnoreCase("set") || args[1].equalsIgnoreCase("ustaw")) {
                if (!cs.hasPermission("ultrapvp.lvl.set")) {
                    String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "no-permission");
                    cs.sendMessage(message);
                    return true;
                }
                Integer howMany = 0;
                try {
                    howMany = Integer.parseInt(args[2]);
                } catch (NumberFormatException e) {
                    String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "bad-value");
                    cs.sendMessage(message.replace("%value%", args[2]));
                    return true;
                }
                Player player = UltraPvp.getInstance().getServer().getPlayer(args[0]);
                if (player == null) {
                    String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "wrong-player");
                    cs.sendMessage(message.replace("%player%", args[0]));
                    return true;
                }
                if (howMany < 0) {
                    String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "lvl-wrong");
                    cs.sendMessage(message.replace("%lvl%", p.getLvl() - howMany + ""));
                    return true;
                }
                if (cs.equals(player)) {
                    setLvl((Player) cs, p, null, howMany, null);
                } else {
                    setLvl((Player) cs, p, UltraPvp.getInstance().getPlayerManager().getPlayer(player), howMany, player.getName());
                }
            } else if (args[0].equalsIgnoreCase("set") || args[0].equalsIgnoreCase("ustaw")) {
                if (!cs.hasPermission("ultrapvp.lvl.set")) {
                    String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "no-permission");
                    cs.sendMessage(message);
                    return true;
                }
                Integer howMany = 0;
                try {
                    howMany = Integer.parseInt(args[2]);
                } catch (NumberFormatException e) {
                    String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "bad-value");
                    cs.sendMessage(message.replace("%value%", args[2]));
                    return true;
                }
                Player player = UltraPvp.getInstance().getServer().getPlayer(args[1]);
                if (player == null) {
                    String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "wrong-player");
                    cs.sendMessage(message.replace("%player%", args[1]));
                    return true;
                }
                if (howMany < 0) {
                    String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "lvl-wrong");
                    cs.sendMessage(message.replace("%lvl%", p.getLvl() - howMany + ""));
                    return true;
                }
                if (cs.equals(player)) {
                    setLvl(cs, p, null, howMany, null);
                } else {
                    setLvl((Player) cs, p, UltraPvp.getInstance().getPlayerManager().getPlayer(player), howMany, player.getName());
                }
            }
        }
        return true;
    }

    public void showLvl(String name, CommandSender cs, GamePlayer p) {
        if (name == null) {
            String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "lvl-player");
            message = message.replace("%lvl%", p.getLvl()+"");
            cs.sendMessage(message);
        } else {
            String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "lvl-other");
            message = message.replace("%lvl%", p.getLvl()+"").replace("%player%", name);
            cs.sendMessage(message);
        }
    }

    public void reset(Player player, GamePlayer p) {
        if (player.getUniqueId().equals(p.getUuid())) {
            p.resetGamePlayer();
            String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "reset");
            player.sendMessage(message);
            if (!UltraPvp.getInstance().getGradeManager().hasGoodGrade(p)) {
                p.setGrade(UltraPvp.getInstance().getGradeManager().getGrade(p));
            }
        } else {
            GamePlayer toReset = UltraPvp.getInstance().getPlayerManager().getPlayer(player);
            toReset.resetGamePlayer();
            String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "reset-other");
            message = message.replace("%player%", player.getName());
            player.sendMessage(message);
            if (!UltraPvp.getInstance().getGradeManager().hasGoodGrade(p)) {
                p.setGrade(UltraPvp.getInstance().getGradeManager().getGrade(p));
            }
        }
    }

    public void setLvl(CommandSender cs, GamePlayer p, GamePlayer who, Integer how, String name) {
        if (who == null) {
            Integer lvlPoints = (int) (Config.DEFAULT_KILLS * how * Config.MULTIPLER);
            p.setLvl(how);
            p.setLvlPoints(lvlPoints);
            if (!UltraPvp.getInstance().getGradeManager().hasGoodGrade(p)) {
                p.setGrade(UltraPvp.getInstance().getGradeManager().getGrade(p));
            }
            String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "lvl-setted-succesfully");
            cs.sendMessage(message.replace("%lvl%", how + "").replace("%player%", cs.getName()));
        } else {
            Integer lvlPoints = (int) (Config.DEFAULT_KILLS * how * Config.MULTIPLER);
            who.setLvl(how);
            who.setLvlPoints(lvlPoints);
            if (!UltraPvp.getInstance().getGradeManager().hasGoodGrade(who)) {
                who.setGrade(UltraPvp.getInstance().getGradeManager().getGrade(who));
            }
            String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "lvl-setted-succesfully");
            cs.sendMessage(message.replace("%lvl%", how + "").replace("%player%", name));
        }
    }

    public void removeLvl(GamePlayer p, CommandSender cs, GamePlayer who, Integer howMany, String name) {
        if (who == null) {
            if (p.getLvl() - howMany < 0) {
                String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "lvl-wrong");
                cs.sendMessage(message.replace("%lvl%", p.getLvl() - howMany + ""));
                return;
            }
            p.decreaseLvl(howMany);
            int lvlPoints = (int) (howMany * Config.MULTIPLER * Config.DEFAULT_KILLS);
            p.decreaseLvlPoints(lvlPoints);
            String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "lvl-removed-succesfully");
            cs.sendMessage(message.replace("%lvl%", howMany + "").replace("%player%", cs.getName()));
            if (!UltraPvp.getInstance().getGradeManager().hasGoodGrade(p)) {
                p.setGrade(UltraPvp.getInstance().getGradeManager().getGrade(p));
            }
            return;
        } else {
            if (who.getLvl() - howMany < 0) {
                String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "lvl-wrong");
                cs.sendMessage(message.replace("%lvl%", who.getLvl() - howMany + ""));
                return;
            }
            who.decreaseLvl(howMany);
            int lvlPoints = (int) (howMany * Config.MULTIPLER * Config.DEFAULT_KILLS);
            who.decreaseLvlPoints(lvlPoints);
            String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "lvl-removed-succesfully");
            cs.sendMessage(message.replace("%lvl%", howMany + "").replace("%player%", name));
            if (!UltraPvp.getInstance().getGradeManager().hasGoodGrade(who)) {
                who.setGrade(UltraPvp.getInstance().getGradeManager().getGrade(who));
            }
            return;
        }
    }

    public void addLvl(final GamePlayer p, final CommandSender cs, final GamePlayer who, final Integer howMany, final boolean rewards, String otherName) {
        if (who == null) {
            if (p.getLvl() + howMany > p.getMaxLvl()) {
                String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "lvl-too-big");
                cs.sendMessage(message.replace("%lvl%", p.getLvl() + howMany + ""));
                return;
            }
            final Integer lvlPoints = (int) (howMany * Config.MULTIPLER * Config.DEFAULT_KILLS);
            p.addLvl(howMany);
            p.addLvlPoints(lvlPoints);
            if (!UltraPvp.getInstance().getGradeManager().hasGoodGrade(p)) {
                p.setGrade(UltraPvp.getInstance().getGradeManager().getGrade(p));
            }
            if (rewards) {
                if (howMany < 100) {
                    UltraPvp.getInstance().getServer().getScheduler().runTaskAsynchronously(UltraPvp.getInstance(), new Runnable() {

                        @Override
                        public void run() {
                            for (int i = 0; i < howMany; i++) {
                                Integer fakeLvl = p.getLvl() - (howMany - i);
                                UltraPvp.getInstance().getRewardManager().getLvlReward(fakeLvl, p.getPermission())
                                .giveReward(fakeLvl, (Player) cs, p);
                            }
                        }
                    });
                } else {
                    UltraPvp.getInstance().getServer().getScheduler().runTaskTimerAsynchronously(UltraPvp.getInstance(),
                            new LvlIncreaser(howMany, p, (Player) cs), 0, 20);
                }
            }
            String msg = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "lvl-added-succesfully");
            cs.sendMessage(msg.replace("%lvl%", howMany + "").replace("%player%", cs.getName()));
        } else {
            if (who.getLvl() + howMany > who.getMaxLvl()) {
                String message = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "lvl-too-big");
                cs.sendMessage(message.replace("%lvl%", who.getLvl() + howMany + ""));
                return;
            }
            final Integer lvlPoints = (int) (howMany * Config.MULTIPLER * Config.DEFAULT_KILLS);
            who.addLvl(howMany);
            who.addLvlPoints(lvlPoints);
            if (!UltraPvp.getInstance().getGradeManager().hasGoodGrade(who)) {
                who.setGrade(UltraPvp.getInstance().getGradeManager().getGrade(who));
            }
            if (rewards) {
                if (howMany < 100) {
                    UltraPvp.getInstance().getServer().getScheduler().runTaskAsynchronously(UltraPvp.getInstance(), new Runnable() {

                        @Override
                        public void run() {
                            for (int i = 0; i < howMany; i++) {
                                Integer fakeLvl = who.getLvl() - (howMany - i);
                                UltraPvp.getInstance().getRewardManager().getLvlReward(fakeLvl, who.getPermission())
                                        .giveReward(fakeLvl, (Player) cs, who);
                            }
                        }
                    });
                } else {
                    UltraPvp.getInstance().getServer().getScheduler().runTaskTimerAsynchronously(UltraPvp.getInstance(),
                            new LvlIncreaser(howMany, who, (Player) cs), 0, 20);
                }
            }
            String msg = UltraPvp.getInstance().getLangConfig().getMessage(p.getLang(), "lvl-added-succesfully");
            cs.sendMessage(msg.replace("%lvl%", howMany + "").replace("%player%", otherName));
        }
    }

    private class LvlIncreaser extends BukkitRunnable {

        private Integer how;
        private GamePlayer p;
        private Player pl;

        public LvlIncreaser(Integer howMany, GamePlayer p, Player player) {
            how = howMany;
            this.p = p;
            pl = player;
        }

        @Override
        public void run() {
            if (how > 50) {
                how -= 50;
                for (int i = 0; i < how; i++) {
                    Integer fakeLvl = p.getLvl() - (how - i);
                    UltraPvp.getInstance().getRewardManager().getLvlReward(fakeLvl, p.getPermission())
                            .giveReward(fakeLvl, pl, p);
                }
                return;
            } else {
                for (int i = 0; i < how; i++) {
                    Integer fakeLvl = p.getLvl() - (how - i);
                    UltraPvp.getInstance().getRewardManager().getLvlReward(fakeLvl, p.getPermission())
                            .giveReward(fakeLvl, pl, p);
                }
                cancel();
                return;
            }
        }
    }

}