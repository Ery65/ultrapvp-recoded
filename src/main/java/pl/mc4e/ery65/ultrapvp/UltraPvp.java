package pl.mc4e.ery65.ultrapvp;

import com.github.games647.scoreboardstats.ScoreboardStats;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import pl.mc4e.ery65.simpleupdater.SimpleUpdater;
import pl.mc4e.ery65.simpleupdater.Updater;
import pl.mc4e.ery65.ultrapvp.api.UltraPvpAPI;
import pl.mc4e.ery65.ultrapvp.commands.AdminCommands;
import pl.mc4e.ery65.ultrapvp.commands.LvlCommands;
import pl.mc4e.ery65.ultrapvp.configuration.Config;
import pl.mc4e.ery65.ultrapvp.configuration.LangConfig;
import pl.mc4e.ery65.ultrapvp.data.Metrics;
import pl.mc4e.ery65.ultrapvp.listeners.ChatListener;
import pl.mc4e.ery65.ultrapvp.listeners.DeathListener;
import pl.mc4e.ery65.ultrapvp.listeners.PermissionsExListener;
import pl.mc4e.ery65.ultrapvp.managers.*;
import pl.mc4e.ery65.ultrapvp.utils.ScoreboardStatsReplacer;
import ru.tehkode.permissions.bukkit.PermissionsEx;

import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

public final class UltraPvp extends JavaPlugin {
    
    private static UltraPvp instance;

    private Permission perm;

    private Economy economy = null;
    
    private PlayerManager pm;

    private GradeManager gm;

    private PointsManager pointsManager;

    private DatabaseManager databaseManager;

    private PermissionManager permMan;

    private RewardManager rwMan;

    private UltraPvpAPI api;

    private LangConfig lang;
    
    public void onEnable() {
        instance = this;

        api = new UltraPvpAPI();

        if (setupPermissions()) {
            pm = new PlayerManager(perm);
            if (Config.DEBUG) System.out.println("Vault Permission found! Player manager created!");
        } else {
            if (Config.DEBUG) System.out.println("Can\'t setup permissions! Do you have vault?");
        }

        if (isPermission()) {
            if (Config.DEBUG) System.out.println("PermissionsEx found! Disabling auto reloading permissions.");
            getServer().getPluginManager().registerEvents(new PermissionsExListener(), instance);
            pm.setAutoReloadPermissions(Boolean.FALSE);
        }
        setupEconomy();
        getServer().getScheduler().runTaskAsynchronously(instance, new Runnable() {
            @Override
            public void run() {
                permMan = new PermissionManager();
                rwMan = new RewardManager();
                gm = new GradeManager();
                databaseManager = new DatabaseManager(Config.DATA_TYPE);
                pointsManager = new PointsManager(Config.POINTS_TYPE);
                lang = new LangConfig();
                for (World w : getServer().getWorlds()) {
                    if (w.getPlayers().size() > 0) {
                        for (Player p : w.getPlayers()) {
                            fakeOnEnable(p);
                        }
                    }
                }

                final Plugin p = getServer().getPluginManager().getPlugin("SimpleUpdater");

                if (p != null && p instanceof SimpleUpdater) {
                    final Timer t = new Timer();
                    t.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            Updater.unloadPlugin(p);
                            new File("plugins" + File.separator + "SimpleUpdater.jar").delete();
                        }
                    }, 5000);
                }

            }
        });
        if (Config.DEBUG) System.out.println("Registering events.");
        getServer().getPluginManager().registerEvents(new DeathListener(), instance);
        getServer().getPluginManager().registerEvents(new ChatListener(), instance);

        getCommand("lvl").setExecutor(new LvlCommands());
        getCommand("ultrapvp").setExecutor(new AdminCommands());
        if (Config.DEBUG) System.out.println("UltraPvp loaded.");

        getServer().getScheduler().scheduleSyncDelayedTask(instance, new Runnable() {
            @Override
            public void run() {
                if (isScoreboardStats()) {
                    ScoreboardStats stats = (ScoreboardStats) getServer().getPluginManager().getPlugin("ScoreboardStats");
            stats.getReplaceManager().register(new ScoreboardStatsReplacer(instance, api, "ultrapvp_lvl", "ultrapvp_kills", "ultrapvp_deaths",
                    "ultrapvp_ks", "ultrapvp_points", "ultrapvp_lvlp", "ultrapvp_maxlvl", "ultrapvp_lsr"), instance, "ultrapvp_lvl",
                    "ultrapvp_kills", "ultrapvp_deaths", "ultrapvp_ks", "ultrapvp_points", "ultrapvp_lvlp", "ultrapvp_maxlvl", "ultrapvp_lsr");
                }
            }
        }, 40L);

        try {
            Metrics metrics = new Metrics(this);
            metrics.start();
        } catch (IOException e) {
            // Failed to submit the stats :-(
        }
    }
    
    public void onDisable() {
        if (Config.DEBUG) System.out.println("Saving all players on end of work.");
        pm.saveAll();
        databaseManager.stop();
    }
    
    private boolean isPermission() {
        try {
            Plugin p = getServer().getPluginManager().getPlugin("PermissionsEx");
            return !(p == null || !(p instanceof PermissionsEx));
        } catch (Exception e) {
            return false;
        }
    }

    private boolean isScoreboardStats() {
        try {
            Plugin p = getServer().getPluginManager().getPlugin("ScoreboardStats");
            return !(p == null || !(p instanceof ScoreboardStats));
        } catch (Exception e) {
            return false;
        }
    }
    
    private boolean setupPermissions() {
        RegisteredServiceProvider<Permission> permissionProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.permission.Permission.class);
        if (permissionProvider != null) {
            perm = permissionProvider.getProvider();
        }
        return (perm != null);
    }

    private boolean setupEconomy() {
        RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economyProvider != null) {
            economy = economyProvider.getProvider();
        }
        return (economy != null);
    }

    public void fakeOnEnable(Player p) {
        if (!pm.containsPlayer(p)) {
            pm.addPlayer(p);
        }
    }

    public UltraPvpAPI getAPI() { return api; }

    public Boolean hasPermission() { return perm != null; }

    public Boolean hasEconomy() { return economy != null; }

    public DatabaseManager getDatabaseManager() { return databaseManager; }

    public PointsManager getPointsManager() { return pointsManager; }

    public GradeManager getGradeManager() { return gm; }

    public RewardManager getRewardManager() { return rwMan; }

    public PermissionManager getPermissionManager() { return permMan; }

    public PlayerManager getPlayerManager() { return pm; }

    public LangConfig getLangConfig() { return lang; }

    public Permission getPermission() { return perm; }

    public Economy getEconomy() { return economy; }
    
    public static UltraPvp getInstance() { return instance; }
    
}