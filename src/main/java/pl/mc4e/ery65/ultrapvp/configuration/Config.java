package pl.mc4e.ery65.ultrapvp.configuration;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import pl.mc4e.ery65.ultrapvp.UltraPvp;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ery65 on 2016-02-10.
 */
public class Config {

    public static String MYSQL_USER, MYSQL_PASSWORD, MYSQL_HOST, MYSQL_DATABASE, POINTS_TYPE, LANGUAGE, DATA_TYPE,
    LVL_NAME, LVL_COLOR, RANG_COLOR, GRADE_NAME, LVL_TITLE;

    public static Integer MYSQL_PORT, DEFAULT_KILLS, MAX_SESSION, ELO_START_POINTS, ELO_KFACTOR1, ELO_KFACTOR2,
    ELO_KFACTOR3, MIN_CHAT_LVL, AUTOSAVE;

    public static Double MULTIPLER;

    public static Boolean ENABLE_CHAT, MAX_LVL_REWARDS, DEBUG;

    public static List<String> LOCKED_WORLDS = new ArrayList<String>();

    public static ConfigurationSection MAX_LEVELS;

    static {
        init();
    }

    private static void init() {
        File config = new File(UltraPvp.getInstance().getDataFolder() + File.separator + "config.yml");
        if (!config.exists()) {
            UltraPvp.getInstance().getDataFolder().mkdirs();
            copy(UltraPvp.getInstance().getResource("Resources/config.default"), config);
        }
        FileConfiguration cfg = YamlConfiguration.loadConfiguration(config);
        LANGUAGE = cfg.getString("Language", "en");
        DATA_TYPE = cfg.getString("Data-type", "sql").toLowerCase();
        POINTS_TYPE = cfg.getString("Points-type", "elo").toLowerCase();
        DEBUG = cfg.getBoolean("Debug", false);
        LOCKED_WORLDS = cfg.getStringList("Locked-Worlds");
        AUTOSAVE = cfg.getInt("Autosave", 300);
        ConfigurationSection chat = cfg.getConfigurationSection("Chat");
        LVL_NAME = chat.getString("lvl-name", "lvl");
        LVL_COLOR = chat.getString("default-lvl-color", "&c");
        RANG_COLOR = chat.getString("default-rang-color", "&2");
        GRADE_NAME = chat.getString("rang-name", "RANG");
        ENABLE_CHAT = chat.getBoolean("enable-default", false);
        MIN_CHAT_LVL = chat.getInt("min-lvl", 0);
        LVL_TITLE = chat.getString("lvl-title", "LvL");
        ConfigurationSection lvl = cfg.getConfigurationSection("Levels");
        DEFAULT_KILLS = lvl.getInt("default-kills", 1);
        MAX_SESSION = lvl.getInt("player-max-kills-session");
        MAX_LEVELS = lvl.getConfigurationSection("max-level");
        MULTIPLER = lvl.getDouble("multipler", 1.0);
        MAX_LVL_REWARDS = lvl.getBoolean("max-level-rewards", true);
        ConfigurationSection mysql = cfg.getConfigurationSection("MySQL");
        MYSQL_PORT = mysql.getInt("port", 3306);
        MYSQL_DATABASE = mysql.getString("database", "minecraft");
        MYSQL_HOST = mysql.getString("host", "localhost");
        MYSQL_USER = mysql.getString("user", "root");
        MYSQL_PASSWORD = mysql.getString("password", "root");
        ConfigurationSection elo = cfg.getConfigurationSection("ELO");
        ELO_START_POINTS = elo.getInt("start-points", 2000);
        ELO_KFACTOR1 = elo.getInt("K-factor1", 32);
        ELO_KFACTOR2 = elo.getInt("K-factor2", 24);
        ELO_KFACTOR3 = elo.getInt("K-factor3", 16);
    }

    public static void reload() {
        LOCKED_WORLDS = new ArrayList<String>();
        init();
    }

    public static void copy(InputStream resource, File to) {
        try {
            FileOutputStream out = new FileOutputStream(to);
            int size = 0;
            byte[] buffer = new byte[1024];

            while ((size = resource.read(buffer)) != -1) {
                out.write(buffer, 0, size);
            }

            out.close();
            resource.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
