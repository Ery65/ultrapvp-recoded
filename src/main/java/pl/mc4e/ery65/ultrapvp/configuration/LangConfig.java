package pl.mc4e.ery65.ultrapvp.configuration;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import pl.mc4e.ery65.ultrapvp.UltraPvp;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import static pl.mc4e.ery65.ultrapvp.configuration.Config.LANGUAGE;
import static pl.mc4e.ery65.ultrapvp.configuration.Config.copy;

/**
 * Created by Ery65 on 2016-02-24.
 */
public class LangConfig {

    private Map<String, Object> messages = new HashMap<String, Object>();

    private Boolean doubleMode = false;

    private String defLang = "pl";

    private final String[] helpString_EN = new String[] {
            ChatColor.COLOR_CHAR + "8#=============" + ChatColor.COLOR_CHAR + "6UltraPvp Help" + ChatColor.COLOR_CHAR + "8=============#",
            ChatColor.COLOR_CHAR + "b/ultrapvp" + ChatColor.COLOR_CHAR + "7- " + ChatColor.COLOR_CHAR + "6shows you current version of plugin",
            ChatColor.COLOR_CHAR + "b/ultrapvp check" + ChatColor.COLOR_CHAR + "7- " + ChatColor.COLOR_CHAR + "6check for newest version of plugin",
            ChatColor.COLOR_CHAR + "b/ultrapvp reload" + ChatColor.COLOR_CHAR + "7- " + ChatColor.COLOR_CHAR + "6reloading configs",
            ChatColor.COLOR_CHAR + "b/ultrapvp update" + ChatColor.COLOR_CHAR + "7- " + ChatColor.COLOR_CHAR + "6downloading newest version (if available)",
            ChatColor.COLOR_CHAR + "b/ultrapvp help" + ChatColor.COLOR_CHAR + "7- " + ChatColor.COLOR_CHAR + "6shows help for you :P",
            ChatColor.COLOR_CHAR + "b/ultrapvp save all" + ChatColor.COLOR_CHAR + "7- " + ChatColor.COLOR_CHAR + "6force save all players",
            ChatColor.COLOR_CHAR + "b/ultrapvp player (name) remove" + ChatColor.COLOR_CHAR + "7- " + ChatColor.COLOR_CHAR + "6removing player from database",
            ChatColor.COLOR_CHAR + "b/ultrapvp player (name) reload" + ChatColor.COLOR_CHAR + "7- " + ChatColor.COLOR_CHAR + "6reloading player data from database ("
                    + ChatColor.COLOR_CHAR + "conly use if something is wrong with player!!!" + ChatColor.COLOR_CHAR + "6)",
            ChatColor.COLOR_CHAR + "b/ultrapvp help 2" + ChatColor.COLOR_CHAR + "7- " + ChatColor.COLOR_CHAR + "6type for next page",
            ChatColor.COLOR_CHAR + "b/ultrapvp player (name) reset" + ChatColor.COLOR_CHAR + "7- " + ChatColor.COLOR_CHAR + "6resets player stats (kills, lvl, etc)",
            ChatColor.COLOR_CHAR + "b/ultrapvp player (name) save" + ChatColor.COLOR_CHAR + "7- " + ChatColor.COLOR_CHAR + "6forcing save a player",
            ChatColor.COLOR_CHAR + "b/ultrapvp player (name) unlock (lvl)" + ChatColor.COLOR_CHAR + "7- " + ChatColor.COLOR_CHAR + "6unlocking lvl limit to (lvl)",
            ChatColor.COLOR_CHAR + "b/ultrapvp player (name) save" + ChatColor.COLOR_CHAR + "7- " + ChatColor.COLOR_CHAR + "6forcing save a player",
            ChatColor.COLOR_CHAR + "b/ultrapvp player (name) points reset" + ChatColor.COLOR_CHAR + "7- " + ChatColor.COLOR_CHAR + "6resets player points",
            ChatColor.COLOR_CHAR + "b/ultrapvp player (name) points add (number)" + ChatColor.COLOR_CHAR + "7- " + ChatColor.COLOR_CHAR + "adding points for player",
            ChatColor.COLOR_CHAR + "b/ultrapvp player (name) points remove (number)" + ChatColor.COLOR_CHAR + "7- " + ChatColor.COLOR_CHAR + "6removing points for player",
            ChatColor.COLOR_CHAR + "b/ultrapvp player (name) points set (number)" + ChatColor.COLOR_CHAR + "7- " + ChatColor.COLOR_CHAR + "6sets player points"
    };

    private final String[] helpString_PL = new String[] {
            ChatColor.COLOR_CHAR + "8#=============" + ChatColor.COLOR_CHAR + "6UltraPvp Pomoc" + ChatColor.COLOR_CHAR + "8=============#",
            ChatColor.COLOR_CHAR + "b/ultrapvp" + ChatColor.COLOR_CHAR + "7- " + ChatColor.COLOR_CHAR + "6wyswietla aktualna wersje pluginu",
            ChatColor.COLOR_CHAR + "b/ultrapvp check" + ChatColor.COLOR_CHAR + "7- " + ChatColor.COLOR_CHAR + "6sprawdza, czy jest dostepna nowa wersja",
            ChatColor.COLOR_CHAR + "b/ultrapvp reload" + ChatColor.COLOR_CHAR + "7- " + ChatColor.COLOR_CHAR + "6przeladowywuje ustawienia",
            ChatColor.COLOR_CHAR + "b/ultrapvp update" + ChatColor.COLOR_CHAR + "7- " + ChatColor.COLOR_CHAR + "6pobiera nowa wersje (jesli dostepna)",
            ChatColor.COLOR_CHAR + "b/ultrapvp help" + ChatColor.COLOR_CHAR + "7- " + ChatColor.COLOR_CHAR + "6wyswietla pomoc :P",
            ChatColor.COLOR_CHAR + "b/ultrapvp save all" + ChatColor.COLOR_CHAR + "7- " + ChatColor.COLOR_CHAR + "6wymusza zapis wszystkich graczy",
            ChatColor.COLOR_CHAR + "b/ultrapvp player (name) remove" + ChatColor.COLOR_CHAR + "7- " + ChatColor.COLOR_CHAR + "6usuwa gracza z bazy danych",
            ChatColor.COLOR_CHAR + "b/ultrapvp player (name) reload" + ChatColor.COLOR_CHAR + "7- " + ChatColor.COLOR_CHAR + "6ponownie pobiera gracza z bazy danych ("
                    + ChatColor.COLOR_CHAR + "cuzywaj tylko w przypadku bledow z graczem!!!" + ChatColor.COLOR_CHAR + "6)",
            ChatColor.COLOR_CHAR + "b/ultrapvp help 2" + ChatColor.COLOR_CHAR + "7- " + ChatColor.COLOR_CHAR + "6przejdz na druga strone",
            ChatColor.COLOR_CHAR + "b/ultrapvp player (name) reset" + ChatColor.COLOR_CHAR + "7- " + ChatColor.COLOR_CHAR + "6resetuje staty gracza (zabicia, poziom, itp)",
            ChatColor.COLOR_CHAR + "b/ultrapvp player (name) save" + ChatColor.COLOR_CHAR + "7- " + ChatColor.COLOR_CHAR + "6forcing save a player",
            ChatColor.COLOR_CHAR + "b/ultrapvp player (name) unlock (lvl)" + ChatColor.COLOR_CHAR + "7- " + ChatColor.COLOR_CHAR + "6unlocking lvl limit to (lvl)",
            ChatColor.COLOR_CHAR + "b/ultrapvp player (name) save" + ChatColor.COLOR_CHAR + "7- " + ChatColor.COLOR_CHAR + "6wymusza zapis gracza",
            ChatColor.COLOR_CHAR + "b/ultrapvp player (name) points reset" + ChatColor.COLOR_CHAR + "7- " + ChatColor.COLOR_CHAR + "6resetuje punkty gracza",
            ChatColor.COLOR_CHAR + "b/ultrapvp player (name) points add (number)" + ChatColor.COLOR_CHAR + "7- " + ChatColor.COLOR_CHAR + "dodaje punkty dla gracza",
            ChatColor.COLOR_CHAR + "b/ultrapvp player (name) points remove (number)" + ChatColor.COLOR_CHAR + "7- " + ChatColor.COLOR_CHAR + "6odejmuje punkty gracza",
            ChatColor.COLOR_CHAR + "b/ultrapvp player (name) points set (number)" + ChatColor.COLOR_CHAR + "7- " + ChatColor.COLOR_CHAR + "6ustawia punkty gracza"
    };

    public LangConfig() {
        load();
    }

    private void load() {
        if (LANGUAGE.toLowerCase().equals("pl") || LANGUAGE.toLowerCase().equals("en")) {
            defLang = LANGUAGE.toLowerCase();
            File dir = new File(UltraPvp.getInstance().getDataFolder() + File.separator + "langs");
            dir.mkdirs();
            File lang = new File(dir + File.separator + LANGUAGE.toLowerCase() + "_" + LANGUAGE.toUpperCase() + ".yml");
            if (!lang.exists()) copy(UltraPvp.getInstance().getResource("Resources/" + LANGUAGE.toLowerCase() + "_" + LANGUAGE.toUpperCase() + ".default"), lang);
            loadMessages(null, lang);
        } else if (LANGUAGE.toLowerCase().equals("all")) {
            doubleMode = true;
            File dir = new File(UltraPvp.getInstance().getDataFolder() + File.separator + "langs");
            dir.mkdirs();
            File lang = new File(dir + File.separator + "pl_PL.yml");
            if (!lang.exists()) copy(UltraPvp.getInstance().getResource("pl_PL.default"), lang);
            loadMessages("pl", lang);
            lang = new File(dir + File.separator + "en_US.yml");
            if (!lang.exists()) copy(UltraPvp.getInstance().getResource("en_US.default"), lang);
            loadMessages("en", lang);
        }
    }

    public void reload() {
        messages = new HashMap<String, Object>();
        load();
    }

    private void loadMessages(String lang, File file) {
        if (lang == null) {
            FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
            messages.put("player-gain-reward", cfg.getString("player-gain-reward").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            messages.put("player-gain-money", cfg.getString("player-gain-money").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            messages.put("player-gain-lvl", cfg.getString("player-gain-lvl").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            messages.put("player-max-lvl-off", cfg.getString("player-max-lvl-off").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            messages.put("player-max-lvl-on", cfg.getString("player-max-lvl-on").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            messages.put("stats-console", cfg.getString("stats-console").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            messages.put("stats-player", cfg.getString("stats-player").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            messages.put("stats-other", cfg.getString("stats-other").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            messages.put("lvls-console", cfg.getString("lvls-console").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            messages.put("lvl-player", cfg.getString("lvl-player").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            messages.put("lvl-other", cfg.getString("lvl-other").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            messages.put("lvl-setted-succesfully", cfg.getString("lvl-setted-succesfully").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            messages.put("lvl-added-succesfully", cfg.getString("lvl-added-succesfully").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            messages.put("lvl-removed-succesfully", cfg.getString("lvl-removed-succesfully").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            messages.put("lvl-wrong", cfg.getString("lvl-wrong").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            messages.put("lvl-too-big", cfg.getString("lvl-too-big").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            messages.put("reset", cfg.getString("reset").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            messages.put("reset-other", cfg.getString("reset-other").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            messages.put("bad-value", cfg.getString("bad-value").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            messages.put("wrong-player", cfg.getString("wrong-player").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            messages.put("no-permission", cfg.getString("no-permission").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            messages.put("chat-disable", cfg.getString("chat-disable").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            messages.put("lvl-usage", cfg.getString("lvl-usage").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            messages.put("no-slot-message", cfg.getString("no-slot-message").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            messages.put("plugin-version", cfg.getString("version").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            messages.put("reload", cfg.getString("reload").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            messages.put("save-all", cfg.getString("save-all").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            messages.put("query-succesfull", cfg.getString("query-succesfull").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            messages.put("cant-when-online", cfg.getString("cant-when-online").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            messages.put("new-version-available", cfg.getString("new-version-available").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            messages.put("good-version", cfg.getString("good-version").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            messages.put("first-check", cfg.getString("first-check").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            messages.put("update-in-progress", cfg.getString("update-in-progress").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
        } else {
            Map<String, String> msg = new HashMap<String, String>();
            FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
            msg.put("player-gain-reward", cfg.getString("player-gain-reward").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            msg.put("player-gain-money", cfg.getString("player-gain-money").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            msg.put("player-gain-lvl", cfg.getString("player-gain-lvl").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            msg.put("player-max-lvl-off", cfg.getString("player-max-lvl-off").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            msg.put("player-max-lvl-on", cfg.getString("player-max-lvl-on").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            msg.put("stats-console", cfg.getString("stats-console").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            msg.put("stats-player", cfg.getString("stats-player").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            msg.put("stats-other", cfg.getString("stats-other").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            msg.put("lvls-console", cfg.getString("lvls-console").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            msg.put("lvl-player", cfg.getString("lvl-player").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            msg.put("lvl-other", cfg.getString("lvl-other").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            msg.put("lvl-setted-succesfully", cfg.getString("lvl-setted-succesfully").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            msg.put("lvl-added-succesfully", cfg.getString("lvl-added-succesfully").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            msg.put("lvl-removed-succesfully", cfg.getString("lvl-removed-succesfully").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            msg.put("lvl-wrong", cfg.getString("lvl-wrong").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            msg.put("lvl-too-big", cfg.getString("lvl-too-big").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            msg.put("reset", cfg.getString("reset").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            msg.put("reset-other", cfg.getString("reset-other").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            msg.put("bad-value", cfg.getString("bad-value").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            msg.put("wrong-player", cfg.getString("wrong-player").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            msg.put("no-permission", cfg.getString("no-permission").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            msg.put("chat-disable", cfg.getString("chat-disable").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            msg.put("lvl-usage", cfg.getString("lvl-usage").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            msg.put("no-slot-message", cfg.getString("no-slot-message").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            msg.put("plugin-version", cfg.getString("version").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            msg.put("reload", cfg.getString("reload").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            msg.put("save-all", cfg.getString("save-all").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            msg.put("query-succesfull", cfg.getString("query-succesfull").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            msg.put("cant-when-online", cfg.getString("cant-when-online").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            msg.put("new-version-available", cfg.getString("new-version-available").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            msg.put("good-version", cfg.getString("good-version").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            msg.put("first-check", cfg.getString("first-check").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            msg.put("update-in-progress", cfg.getString("update-in-progress").replace('&', ChatColor.COLOR_CHAR).replace("\\n", "\n"));
            messages.put(lang, msg);
        }
    }

    public String getMessage(String lang, String message) {
        if (lang != null) {
            if (messages.containsKey(lang)) {
                if (((Map<String, String>)messages.get(lang)).containsKey(message)) {
                    return (String) ((Map<String, String>)messages.get(lang)).get(message);
                } else {
                    return "§cError! Can't find message " + message;
                }
            } else {
                if (doubleMode) {
                    if (messages.containsKey(defLang)) {
                        if (((Map<String, String>)messages.get(defLang)).containsKey(message)) {
                            return (String) ((Map<String, String>)messages.get(defLang)).get(message);
                        } else {
                            return "§cError! Can't find message " + message;
                        }
                    } else {
                        return "§cError! Can't find message to lang " + defLang;
                    }
                }
                return "§cError! Can't find message to lang " + lang;
            }
        } else {
            if (doubleMode) {
                if (messages.containsKey(defLang)) {
                    if (((Map<String, String>)messages.get(defLang)).containsKey(message)) {
                        return (String) ((Map<String, String>)messages.get(defLang)).get(message);
                    } else {
                        return "§cError! Can't find message " + message;
                    }
                } else {
                    return "§cError! Can't find message to lang " + defLang;
                }
            } else {
                if (messages.containsKey(message)) {
                    return (String) messages.get(message);
                } else {
                    return "§cError! Can't find message " + message;
                }
            }
        }
    }

    public String[] getHelp(String lang, int page, Boolean console) {
        if (lang == null || lang.equalsIgnoreCase("all")) lang = defLang;
        if (lang.equalsIgnoreCase("pl")) {
            String[] help = new String[10];
            help[0] = helpString_PL[0];
            if (page == 0 || page == 1) {
                if (console) {
                    for (int i = 1; i < 10; i++) {
                        help[i] = helpString_PL[i].replace("/", "");
                    }
                } else {
                    for (int i = 1; i < 10; i++) {
                        help[i] = helpString_PL[i];
                    }
                }
            } else {
                if (console) {
                    for (int i = 10; i < helpString_PL.length; i++) {
                        help[i] = helpString_PL[i].replace("/", "");
                    }
                } else {
                    for (int i = 10; i < helpString_PL.length; i++) {
                        help[i] = helpString_PL[i];
                    }
                }
            }
            return help;
        } else if (lang.equalsIgnoreCase("en")) {
            String[] help = new String[10];
            help[0] = helpString_EN[0];
            if (page == 0 || page == 1) {
                if (console) {
                    for (int i = 1; i < 10; i++) {
                        help[i] = helpString_EN[i].replace("/", "");
                    }
                } else {
                    for (int i = 1; i < 10; i++) {
                        help[i] = helpString_EN[i];
                    }
                }
            } else {
                if (console) {
                    for (int i = 10; i < helpString_EN.length; i++) {
                        help[i] = helpString_EN[i].replace("/", "");
                    }
                } else {
                    for (int i = 10; i < helpString_EN.length; i++) {
                        help[i] = helpString_EN[i];
                    }
                }
            }
            return help;
        } else {
            return new String[]{ChatColor.COLOR_CHAR + "cHelp message not found for lang " +
                    ChatColor.COLOR_CHAR + "6" + lang + ChatColor.COLOR_CHAR + "c!"};
        }
    }

}
