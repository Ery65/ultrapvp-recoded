package pl.mc4e.ery65.ultrapvp.managers;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;
import pl.mc4e.ery65.ultrapvp.UltraPvp;
import pl.mc4e.ery65.ultrapvp.configuration.Config;
import pl.mc4e.ery65.ultrapvp.utils.GamePlayer;
import pl.mc4e.ery65.ultrapvp.utils.ItemsUtil;
import pl.mc4e.ery65.ultrapvp.utils.SimpleReward;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Ery65 on 2016-02-11.
 */
public class RewardManager {

    private Map<Integer, Map<String, SimpleReward>> lvlRewards = new HashMap<Integer, Map<String, SimpleReward>>();
    private Map<Integer, Map<String, SimpleReward>> specialRewards = new HashMap<Integer, Map<String, SimpleReward>>();

    private HashMap<String, Integer> maxLvls = new HashMap<String, Integer>();

    private  Integer maxLvl = 1;

    private List<Integer> levelPoints = new ArrayList<Integer>();

    public RewardManager() {
        File file = new File(UltraPvp.getInstance().getDataFolder() + File.separator + "rewards.yml");
        if (!file.exists()) {
            UltraPvp.getInstance().getDataFolder().mkdirs();
            Config.copy(UltraPvp.getInstance().getResource("Resources/rewards.default"), file);
        }
        FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
        ConfigurationSection defRewards = cfg.getConfigurationSection("Default-Rewards");
        maxLvls.put("default", 10000);
        for (String s : Config.MAX_LEVELS.getKeys(false)) {
            maxLvls.put(s.toLowerCase(), Config.MAX_LEVELS.getInt(s, 10000));
            if (Config.MAX_LEVELS.getInt(s, 10000) > maxLvl) {
                maxLvl = Config.MAX_LEVELS.getInt(s, 10000);
            }
        }
        for (String group : defRewards.getKeys(false)) {
            for (String codeLvl : defRewards.getConfigurationSection(group).getKeys(false)) {
                String[] lvls = codeLvl.split("-");
                Integer min = Integer.valueOf(lvls[0]);
                Integer max = Integer.valueOf(lvls[1]);
                ItemStack[] items = ItemsUtil.parseItemStacksFromList(defRewards.getStringList(group + "." + codeLvl + ".items"));
                List<String> commands = defRewards.getStringList(group + "." + codeLvl + ".commands");
                Double money = defRewards.getDouble(group + "." + codeLvl + ".money", 0.0);
                SimpleReward reward = new SimpleReward(items, commands, money);
                for (Integer i = min; i <= max; i++) {
                    if (lvlRewards.containsKey(i)) {
                        if (lvlRewards.get(i).containsKey(group)) {
                            throw new Error("Double entry for default rewards!! (group " + group + " with lvl " + i + ")");
                        } else {
                            lvlRewards.get(i).put(group, reward);
                        }
                    } else {
                        lvlRewards.put(i, new HashMap<String, SimpleReward>());
                        lvlRewards.get(i).put(group, reward);
                    }
                }
            }
        }
        ConfigurationSection specialRew = cfg.getConfigurationSection("Special-Rewards");
        for (String group : specialRew.getKeys(false)) {
            for (String codeLvl : specialRew.getConfigurationSection(group).getKeys(false)) {
                Integer lvl = Integer.valueOf(codeLvl);
                ItemStack[] items = ItemsUtil.parseItemStacksFromList(specialRew.getStringList(group + "." + codeLvl + ".items"));
                List<String> commands = specialRew.getStringList(group + "." + codeLvl + ".commands");
                Double money = specialRew.getDouble(group + "." + codeLvl + ".money", 0.0);
                SimpleReward reward = new SimpleReward(items, commands, money);
                if (specialRewards.containsKey(lvl)) {
                    if (specialRewards.get(lvl).containsKey(group)) {
                        throw new Error("Double entry for special rewards!! (group " + group + " with lvl " + lvl + ")");
                    } else {
                        specialRewards.get(lvl).put(group, reward);
                    }
                } else {
                    specialRewards.put(lvl, new HashMap<String, SimpleReward>());
                    specialRewards.get(lvl).put(group, reward);
                }
            }
        }
        for (Integer i = 0; i <= maxLvl; i++) {
            levelPoints.add((int)(Config.DEFAULT_KILLS * i * Config.MULTIPLER));
        }
    }

    public Boolean canIncreaseLvl(Integer lvlPoints) { return levelPoints.contains(lvlPoints); }

    public Boolean canIncreaseLvl(GamePlayer player) { return canIncreaseLvl(player.getLvlPoints()); }

    public Boolean containsReward(GamePlayer player) { return containsReward(player.getLvl(), player.getPermission()); }

    public Boolean containsReward(Integer lvlPoints, String permission) {
        if (lvlRewards.containsKey(lvlPoints)) {
            return (lvlRewards.get(lvlPoints).containsKey(permission)) ? Boolean.TRUE : lvlRewards.get(lvlPoints).containsKey("default");
        }
        return Boolean.FALSE;
    }

    public Boolean containsSpecialReward(GamePlayer player) { return containsSpecialReward(player.getLvl(), player.getPermission()); }

    public Boolean containsSpecialReward(Integer lvlPoints, String permission) {
        if (specialRewards.containsKey(lvlPoints)) {
            System.out.println("specialRewards contains lvl " + lvlPoints);
            return (specialRewards.get(lvlPoints).containsKey(permission)) ? Boolean.TRUE : specialRewards.get(lvlPoints).containsKey("default");
        }
        return Boolean.FALSE;
    }

    public SimpleReward getLvlReward(GamePlayer player) { return getLvlReward(player.getKills(), player.getPermission()); }

    public SimpleReward getLvlReward(Integer lvlPoints, String permission) {
        if (lvlRewards.get(lvlPoints).containsKey(permission)) {
            return lvlRewards.get(lvlPoints).get(permission);
        } else return lvlRewards.get(lvlPoints).get("default");
    }

    public SimpleReward getSpecialReward(GamePlayer player) { return getSpecialReward(player.getKills(), player.getPermission()); }

    public SimpleReward getSpecialReward(Integer lvlPoints, String permission) {
        if (specialRewards.get(lvlPoints).containsKey(permission)) {
            return specialRewards.get(lvlPoints).get(permission);
        } else return specialRewards.get(lvlPoints).get("default");
    }

    public Boolean isSpecialReward(SimpleReward reward) {
        for (Integer key : specialRewards.keySet()) {
            for (SimpleReward r : specialRewards.get(key).values()) {
                if (r.equals(reward)) {
                    return Boolean.TRUE;
                }
            }
        }
        return Boolean.FALSE;
    }

    public Integer getMaxLvl(String group) {
        if (maxLvls.containsKey(group.toLowerCase())) {
            return maxLvls.get(group.toLowerCase());
        } else {
            return maxLvls.get("default");
        }
    }

    public Integer getTotalMaxLvl() {
        return maxLvl;
    }

}
