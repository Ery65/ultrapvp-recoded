package pl.mc4e.ery65.ultrapvp.managers;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import pl.mc4e.ery65.ultrapvp.UltraPvp;
import pl.mc4e.ery65.ultrapvp.configuration.Config;
import pl.mc4e.ery65.ultrapvp.utils.GamePlayer;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ery65 on 2016-02-12.
 */
public class GradeManager {

    private Map<Integer, String> grades = new HashMap<Integer, String>();

    public GradeManager() {
        File file = new File(UltraPvp.getInstance().getDataFolder() + File.separator + "grades.yml");
        if (!file.exists()) {
            UltraPvp.getInstance().getDataFolder().mkdirs();
            Config.copy(UltraPvp.getInstance().getResource("Resources/grades.default"), file);
        }
        FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
        ConfigurationSection grads = cfg.getConfigurationSection("Grades");
        for (String grade : grads.getKeys(false)) {
            String[] splitted = grads.getString(grade).split("-");
            if (splitted.length == 1) {
                grades.put(Integer.valueOf(splitted[0]), grade);
            } else {
                Integer min = Integer.valueOf(splitted[0]);
                Integer max = Integer.valueOf(splitted[1]);
                for (Integer i = min; i <= max; i++) {
                    grades.put(i, grade);
                }
            }
        }
    }

    public Boolean hasGoodGrade(String grade, Integer lvl) { return (grades.containsKey(lvl)) ? grades.get(lvl).equals(grade) : true; }

    public Boolean hasGoodGrade(GamePlayer player) { return hasGoodGrade(player.getGrade(), player.getLvl()); }

    public String getGrade(Integer lvl) { return (grades.containsKey(lvl)) ? grades.get(lvl) : ""; }

    public String getGrade(GamePlayer player) { return getGrade(player.getLvl()); }

}