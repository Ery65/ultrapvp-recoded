package pl.mc4e.ery65.ultrapvp.managers;

import net.milkbowl.vault.permission.Permission;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import pl.mc4e.ery65.ultrapvp.UltraPvp;
import pl.mc4e.ery65.ultrapvp.configuration.Config;

import java.io.File;
import java.util.Map;
import java.util.HashMap;

/**
 * Created by Ery65 on 2016-02-10.
 */
public class PermissionManager {

    private Map<String, Integer> groupRank = new HashMap<String, Integer>();
    private Map<Integer, Integer> moneyDiffrence = new HashMap<Integer, Integer>();
    private Map<Integer, Integer> itemsDiffrence = new HashMap<Integer, Integer>();

    public PermissionManager() {
        File file = new File(UltraPvp.getInstance().getDataFolder() + File.separator + "permissions.yml");
        if (!file.exists()) {
            UltraPvp.getInstance().getDataFolder().mkdirs();
            Config.copy(UltraPvp.getInstance().getResource("Resources/permission.default"), file);
        }
        FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);

        ConfigurationSection groupsInfo = cfg.getConfigurationSection("Groups");

        for (String key : groupsInfo.getKeys(false)) {
            groupRank.put(key.toLowerCase(), groupsInfo.getInt(key, 0));
        }

        ConfigurationSection diff = cfg.getConfigurationSection("Diffrence");
        for (String key : diff.getKeys(false)) {
            Integer diffrence = Integer.valueOf(key);
            Integer item = diff.getInt(key + ".item", 0);
            Integer money = diff.getInt(key + ".money", 0);
            moneyDiffrence.put(diffrence, money);
            itemsDiffrence.put(diffrence, item);
        }
    }

    public Integer getGroupRank(String group) {
        group = group.toLowerCase();
        if (!groupRank.containsKey(group)) return 0;
        else return groupRank.get(group);
    }

    public String getHighestPermission(Player player, Permission permission) {
        String perm = "default";
        Integer lastRank = (groupRank.containsKey(perm)) ? groupRank.get(perm) : 0;
        for (String s : permission.getPlayerGroups(player)) {
            if (groupRank.containsKey(s.toLowerCase())) {
                if (groupRank.get(s.toLowerCase()) > lastRank) {
                    perm = s.toLowerCase();
                }
            }
        }
        return perm;
    }

    public Integer compareGroups(String killerGroup, String entityGroup) {
        return  getGroupRank(entityGroup) - getGroupRank(killerGroup);
    }

    public Integer getAdditionalMoney(Integer comparedGroups) {
        if (!moneyDiffrence.containsKey(comparedGroups)) return 0;
        else return moneyDiffrence.get(comparedGroups);
    }

    public Integer getAdditionalItems(Integer comparedGroups) {
        if (!itemsDiffrence.containsKey(comparedGroups)) return 0;
        else return itemsDiffrence.get(comparedGroups);
    }

}
