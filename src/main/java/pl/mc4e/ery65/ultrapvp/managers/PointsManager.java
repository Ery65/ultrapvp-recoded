package pl.mc4e.ery65.ultrapvp.managers;

import pl.mc4e.ery65.ultrapvp.configuration.Config;
import pl.mc4e.ery65.ultrapvp.utils.GamePlayer;

/**
 * Created by Ery65 on 2016-02-13.
 */
public class PointsManager {

    private Integer maxKfPoints, minKfPoints, twentyPercent;

    private Boolean elo = Boolean.FALSE;

    public PointsManager(String pointType) {
        if (pointType.equals("elo")) {
            elo = Boolean.TRUE;
            maxKfPoints = (int) (Config.ELO_START_POINTS * 1.05);
            minKfPoints = (int) (Config.ELO_START_POINTS * 1.2);
            twentyPercent = (int) (Config.ELO_START_POINTS * 0.2);
        }
    }

    public void addPoints(GamePlayer winner, GamePlayer looser) {
        if (elo) {
            Integer powchanceW = 10 ^ ((int)(winner.getPoints() / twentyPercent));
            Integer powchanceL = 10 ^ ((int)(looser.getPoints() / twentyPercent));
            Double chanceW = (double) (powchanceW / (powchanceW + powchanceL));
            Double chanceL = (double) (powchanceL / (powchanceW + powchanceL));
            Integer winnerELO = (int) (winner.getPoints() + getKfactor(winner.getPoints()) * (1 - chanceW));
            Integer looserELO = (int) (looser.getPoints() + getKfactor(looser.getPoints()) * (0 - chanceL));
            winner.setPoints(winnerELO);
            looser.setPoints(looserELO);
        } else {
            winner.addPoints(1);
            looser.decreasePoints(1);
        }
    }

    public Integer getKfactor(Integer points) {
        if (points <= maxKfPoints) {
            return Config.ELO_KFACTOR1;
        } else if (points < maxKfPoints && points <= minKfPoints) {
            return Config.ELO_KFACTOR2;
        } else return Config.ELO_KFACTOR3;
    }

}
