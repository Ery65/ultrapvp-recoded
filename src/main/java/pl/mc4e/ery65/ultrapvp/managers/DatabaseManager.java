package pl.mc4e.ery65.ultrapvp.managers;

import org.bukkit.entity.Player;
import pl.mc4e.ery65.ultrapvp.data.SQLDatabase;
import pl.mc4e.ery65.ultrapvp.utils.GamePlayer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Ery65 on 2016-02-13.
 */
public class DatabaseManager extends SQLDatabase {

    public DatabaseManager(String dataType) {
        super(dataType);
    }

    public Boolean isPlayerExists(UUID uuid) {
        if (mySQLConnection != null) {
            checkMySQLConnection();
            PreparedStatement st = null;
            ResultSet rs = null;
            try {
                st = sqlConnection.prepareStatement("SELECT * FROM `ultrapvp_players` WHERE `uuid`=\'" + uuid.toString() + "\'");
                rs = st.executeQuery();
                if (rs.next()) {
                    return Boolean.TRUE;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                closeResources(st, rs);
            }
        }
        if (sqlConnection != null) {
            checkSQLConnection();
            PreparedStatement st = null;
            ResultSet rs = null;
            try {
                st = sqlConnection.prepareStatement("SELECT * FROM `ultrapvp_players` WHERE `uuid`=\'" + uuid.toString() + "\'");
                rs = st.executeQuery();
                if (rs.next()) {
                    return Boolean.TRUE;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                closeResources(st, rs);
            }
        }
        return Boolean.FALSE;
    }

    public Boolean isPlayerExists(Player player) {
        return isPlayerExists(player.getUniqueId());
    }

    public void addNewPlayer(UUID uuid) {
        if (mySQLConnection != null) {
            checkMySQLConnection();
            PreparedStatement st = null;
            try {
                st = mySQLConnection.prepareStatement("INSERT INTO `ultrapvp_players` (`uuid`) VALUES (\'" + uuid.toString() + "\')");
                st.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                closeResources(st, null);
            }
        }
        if (sqlConnection != null) {
            checkSQLConnection();
            PreparedStatement st = null;
            try {
                st = sqlConnection.prepareStatement("INSERT INTO `ultrapvp_players` (`uuid`) VALUES (\'" + uuid.toString() + "\')");
                st.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                closeResources(st, null);
            }
        }
    }

    public void addNewPlayer(GamePlayer player) {
        addNewPlayer(player.getUuid());
    }

    public void addNewPlayer(Player player) {
        addNewPlayer(player.getUniqueId());
    }

    public void savePlayer(final GamePlayer player) {
        if (player == null) return;
        if (mySQLConnection != null) {
            checkMySQLConnection();
            PreparedStatement st = null;
            try {
                st = mySQLConnection.prepareStatement("UPDATE `ultrapvp_players` SET `kills`=?, `deaths`=?, `lvl`=?, " +
                        "`points`=?, `lvlpoints`=?, `lastreward`=?, `maxlevel`=? WHERE `uuid`=?");
                st.setInt(1, player.getKills());
                st.setInt(2, player.getDeaths());
                st.setInt(3, player.getLvl());
                st.setInt(4, player.getPoints());
                st.setInt(5, player.getLvlPoints());
                st.setInt(6, player.getLastSpecialReward());
                st.setInt(7, player.getMaxLvl());
                st.setString(8, player.getUuid().toString());
                st.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                closeResources(st, null);
            }
        }
        if (sqlConnection != null) {
            checkSQLConnection();
            PreparedStatement st = null;
            try {
                st = sqlConnection.prepareStatement("UPDATE `ultrapvp_players` SET `kills`=?, `deaths`=?, `lvl`=?, " +
                        "`points`=?, `lvlpoints`=?, `lastreward`=?, `maxlevel`=? WHERE `uuid`=?");
                st.setInt(1, player.getKills());
                st.setInt(2, player.getDeaths());
                st.setInt(3, player.getLvl());
                st.setInt(4, player.getPoints());
                st.setInt(5, player.getLvlPoints());
                st.setInt(6, player.getLastSpecialReward());
                st.setInt(7, player.getMaxLvl());
                st.setString(8, player.getUuid().toString());
                st.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                closeResources(st, null);
            }
        }
    }

    public void loadPlayer(GamePlayer player) {
        GamePlayer tempSQL = new GamePlayer(UUID.randomUUID());
        GamePlayer tempMySQL = new GamePlayer(UUID.randomUUID());
        if (mySQLConnection != null) {
            checkMySQLConnection();
            PreparedStatement st = null;
            ResultSet rs = null;
            try {
                st = mySQLConnection.prepareStatement("SELECT * FROM `ultrapvp_players` WHERE `uuid`=\'" + player.getUuid().toString() + "\'");
                rs = st.executeQuery();
                if (rs.next()) {
                    tempMySQL.setKills(rs.getInt("kills"));
                    tempMySQL.setDeaths(rs.getInt("deaths"));
                    tempMySQL.setLvl(rs.getInt("lvl"));
                    tempMySQL.setPoints(rs.getInt("points"));
                    tempMySQL.setLvlPoints(rs.getInt("lvlpoints"));
                    tempMySQL.setLastSpecialReward(rs.getInt("lastreward"));
                    tempMySQL.setMaxLvl(rs.getInt("maxlevel"));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                closeResources(st, rs);
            }
        }
        if (sqlConnection != null) {
            checkSQLConnection();
            PreparedStatement st = null;
            ResultSet rs = null;
            try {
                st = sqlConnection.prepareStatement("SELECT * FROM `ultrapvp_players` WHERE `uuid`=\'" + player.getUuid().toString() + "\'");
                rs = st.executeQuery();
                if (rs.next()) {
                    tempSQL.setKills(rs.getInt("kills"));
                    tempSQL.setDeaths(rs.getInt("deaths"));
                    tempSQL.setLvl(rs.getInt("lvl"));
                    tempSQL.setPoints(rs.getInt("points"));
                    tempSQL.setLvlPoints(rs.getInt("lvlpoints"));
                    tempSQL.setLastSpecialReward(rs.getInt("lastreward"));
                    tempSQL.setMaxLvl(rs.getInt("maxlevel"));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                closeResources(st, rs);
            }
        }
        if (tempMySQL.compare(tempSQL)) player.loadFromPlayer(tempMySQL);
        else player.loadFromPlayer(tempSQL);
    }

    public Map<UUID, Integer> getTopPlayers() {
        return getTopPlayers(10);
    }

    public Map<UUID, Integer> getTopPlayers(int limit) {
        Map<UUID, Integer> top = new HashMap<UUID, Integer>();
        if (mySQLConnection != null) {
            checkMySQLConnection();
            PreparedStatement st = null;
            ResultSet rs = null;
            try {
                st = mySQLConnection.prepareStatement("SELECT `uuid`, `points` FROM `ultrapvp_players` ORDER BY `points` DESC LIMIT " +  limit);
                rs = st.executeQuery();
                while (rs.next()) {
                    top.put(UUID.fromString(rs.getString("uuid")), rs.getInt("points"));
                }
                return top;
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                closeResources(st, rs);
            }
        }
        if (sqlConnection != null) {
            checkSQLConnection();
            PreparedStatement st = null;
            ResultSet rs = null;
            try {
                st = sqlConnection.prepareStatement("SELECT `uuid`, `points` FROM `ultrapvp_players` ORDER BY `points` DESC LIMIT " +  limit);
                rs = st.executeQuery();
                while (rs.next()) {
                    top.put(UUID.fromString(rs.getString("uuid")), rs.getInt("points"));
                }
                return top;
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                closeResources(st, rs);
            }
        }
        return top;
    }

    public void executeQuery(String query) {
        if (mySQLConnection != null) {
            checkMySQLConnection();
            PreparedStatement st = null;
            try {
                st = mySQLConnection.prepareStatement(query);
                st.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                closeResources(st, null);
            }
        }
        if (sqlConnection != null) {
            checkSQLConnection();
            PreparedStatement st = null;
            try {
                st = sqlConnection.prepareStatement(query);
                st.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                closeResources(st, null);
            }
        }
    }

    public void stop() {
        if (sqlConnection != null) closeSQLConnection();
        if (mySQLConnection != null) closeMySQLConnection();
        try {
            super.finalize();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

}