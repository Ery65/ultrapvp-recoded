package pl.mc4e.ery65.ultrapvp.managers;

import java.util.HashMap;
import java.util.UUID;

import net.milkbowl.vault.permission.Permission;

import org.bukkit.entity.Player;

import org.bukkit.scheduler.BukkitTask;
import pl.mc4e.ery65.ultrapvp.UltraPvp;
import pl.mc4e.ery65.ultrapvp.utils.GamePlayer;

public class PlayerManager {
    
    private HashMap<UUID, GamePlayer> players = new HashMap<UUID, GamePlayer>();

    private Boolean autoReloadPermissions;

    private Permission permission;

    private BukkitTask task;
    
    public PlayerManager(final Permission perm) {
        autoReloadPermissions = Boolean.TRUE;
        permission = perm;
        task = UltraPvp.getInstance().getServer().getScheduler().runTaskTimerAsynchronously(UltraPvp.getInstance(), new Runnable() {
            @Override
            public void run() {
                for (UUID u : players.keySet()) {
                    final Player bukkitPlayer = UltraPvp.getInstance().getServer().getPlayer(u);
                    players.get(u).setPermission(UltraPvp.getInstance().getPermissionManager().getHighestPermission(bukkitPlayer, perm));
                }
            }
        }, 0, 6000);
    }
    
    public void addPlayer(final Player player) {
        if (!players.containsKey(player.getUniqueId())) {
            players.put(player.getUniqueId(), new GamePlayer(player.getUniqueId()));
            players.get(player.getUniqueId()).setPermission(UltraPvp.getInstance().getPermissionManager().getHighestPermission(player, permission));
            if (!UltraPvp.getInstance().getDatabaseManager().isPlayerExists(player.getUniqueId())) {
                UltraPvp.getInstance().getServer().getScheduler().runTaskAsynchronously(UltraPvp.getInstance(), new Runnable() {
                    @Override
                    public void run() {
                        UltraPvp.getInstance().getDatabaseManager().addNewPlayer(player);
                        players.get(player.getUniqueId()).setGrade(UltraPvp.getInstance().getGradeManager().getGrade(0));
                        players.get(player.getUniqueId()).setMaxLvl(UltraPvp.getInstance().getRewardManager().getMaxLvl(players.get(player.getUniqueId()).getGrade()));
                    }
                });
            } else {
                UltraPvp.getInstance().getServer().getScheduler().runTaskAsynchronously(UltraPvp.getInstance(), new Runnable() {
                    @Override
                    public void run() {
                        UltraPvp.getInstance().getDatabaseManager().loadPlayer(players.get(player.getUniqueId()));
                        players.get(player.getUniqueId()).setGrade(UltraPvp.getInstance().getGradeManager().getGrade(players.get(player.getUniqueId())));
                        players.get(player.getUniqueId()).setMaxLvl(UltraPvp.getInstance().getRewardManager().getMaxLvl(players.get(player.getUniqueId()).getGrade()));
                    }
                });
            }
        }
    }

    public Boolean containsPlayer(UUID uuid) { return players.containsKey(uuid); }

    public Boolean containsPlayer(Player player) { return containsPlayer(player.getUniqueId()); }

    public GamePlayer getPlayer(UUID uuid) { return players.get(uuid); }

    public GamePlayer getPlayer(Player player) { return getPlayer(player.getUniqueId()); }

    public void removePlayer(final UUID uuid) {
        UltraPvp.getInstance().getServer().getScheduler().runTaskAsynchronously(UltraPvp.getInstance(), new Runnable() {
            @Override
            public void run() {
                UltraPvp.getInstance().getDatabaseManager().savePlayer(players.get(uuid));
                players.remove(uuid);
            }
        });
    }

    public void removePlayer(Player player) { removePlayer(player.getUniqueId()); }

    public void setAutoReloadPermissions(Boolean toggle) {
        if (!autoReloadPermissions) task.cancel();
        if (autoReloadPermissions) {
            task = UltraPvp.getInstance().getServer().getScheduler().runTaskTimerAsynchronously(UltraPvp.getInstance(), new Runnable() {
                @Override
                public void run() {
                    for (UUID u : players.keySet()) {
                        Player bukkitPlayer = UltraPvp.getInstance().getServer().getPlayer(u);
                        players.get(u).setPermission(UltraPvp.getInstance().getPermissionManager()
                                .getHighestPermission(bukkitPlayer, UltraPvp.getInstance().getPermission()));
                    }
                }
            }, 0, 6000);
        }
        autoReloadPermissions = toggle;
    }

    public void saveAll() {
        for (GamePlayer player : players.values()) {
            UltraPvp.getInstance().getDatabaseManager().savePlayer(player);
        }
    }

}
