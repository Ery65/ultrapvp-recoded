package pl.mc4e.ery65.ultrapvp.data;

import pl.mc4e.ery65.ultrapvp.UltraPvp;
import pl.mc4e.ery65.ultrapvp.configuration.Config;

import java.sql.*;

/**
 * Created by Ery65 on 2016-02-13.
 */
public class MySQLDatabase {

    protected Connection mySQLConnection;

    private Long lastMySQLRefresh;

    public MySQLDatabase(String dataType) {
        if (dataType.equals("sql")) return;
        if (Config.DEBUG) System.out.println("Initialize MySQL database.");
        openMySQLConnection();
        checkTable();
    }

    public void openMySQLConnection() {
        try { Class.forName("com.mysql.jdbc.Driver"); } catch (ClassNotFoundException e) { e.printStackTrace(); return; }
        try {
            mySQLConnection = DriverManager.getConnection("jdbc:mysql://" + Config.MYSQL_HOST + ":" + Config.MYSQL_HOST
                    + "/" + Config.MYSQL_DATABASE, Config.MYSQL_USER, Config.MYSQL_PASSWORD);
            lastMySQLRefresh = System.currentTimeMillis();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void checkTable() {
        PreparedStatement st = null;
        try {
            st = mySQLConnection.prepareStatement("CREATE TABLE IF NOT EXISTS `ultrapvp_players` (" +
                    "`id` INTEGER(10) NOT NULL AUTO_INCREMENT, `uuid` CHAR(36) NOT NULL UNIQUE," +
                    "`kills` INTEGER(10) DEFAULT 0, `deaths` INTEGER(10) DEFAULT 0, " +
                    "`lvl` INTEGER(10) DEFAULT 0, `points` INTEGER(10) DEFAULT " +
                    ((Config.POINTS_TYPE.equalsIgnoreCase("elo")) ? Config.ELO_START_POINTS : 0) + ", " +
                    "`lvlpoints` INTEGER(10) DEFAULT 0, `lastreward` INTEGER(10) DEFAULT 0, " +
                    "`maxlevel` INTEGER(10) DEFAULT " + UltraPvp.getInstance().getRewardManager().getMaxLvl("default") + ", " +
                    "PRIMARY KEY (`id`), KEY(`uuid`))");
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(st, null);
        }
    }

    public void closeMySQLConnection() {
        try {
            if (mySQLConnection != null && !mySQLConnection.isClosed()) mySQLConnection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void checkMySQLConnection() {
        if (System.currentTimeMillis() - lastMySQLRefresh > 300000) if (!isValidMySQLConnection()) makeGoodMySQLConnection();
    }

    public Boolean isValidMySQLConnection() {
        if (mySQLConnection == null) return false;
        try { return mySQLConnection.isValid(1); } catch (SQLException e) { e.printStackTrace(); return Boolean.FALSE; }
    }

    public void makeGoodMySQLConnection() {
        try {
            if (mySQLConnection != null && !mySQLConnection.isClosed()) {
                closeMySQLConnection();
                openMySQLConnection();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            openMySQLConnection();
        }
    }

    public void closeResources(Statement st, ResultSet rs) {
        if (st != null) try { st.close(); } catch (SQLException e) { e.printStackTrace(); }
        if (rs != null) try { rs.close(); } catch (SQLException e) { e.printStackTrace(); }
    }
}
