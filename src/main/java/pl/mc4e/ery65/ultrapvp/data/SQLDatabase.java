package pl.mc4e.ery65.ultrapvp.data;

import pl.mc4e.ery65.ultrapvp.UltraPvp;
import pl.mc4e.ery65.ultrapvp.configuration.Config;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by Ery65 on 2016-02-13.
 */
public class SQLDatabase extends MySQLDatabase {

    protected Connection sqlConnection;

    private Long lastSQLRefresh;

    public SQLDatabase(String dataType) {
        super(dataType);
        if (dataType.equals("mysql")) return;
        if (Config.DEBUG) System.out.println("Initialize SQL database.");
        File base = new File(UltraPvp.getInstance().getDataFolder() + File.separator + "database.db");
        if (!base.exists()) {
            UltraPvp.getInstance().getDataFolder().mkdirs();
            try {
                base.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        openSQLConnection();
        checkTable();
    }


    public void openSQLConnection() {
        try { Class.forName("org.sqlite.JDBC"); } catch (ClassNotFoundException e) { e.printStackTrace(); return; }
        try {
            sqlConnection = DriverManager.getConnection("jdbc:sqlite:" + UltraPvp.getInstance().getDataFolder() + File.separator + "database.db");
            lastSQLRefresh = System.currentTimeMillis();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void checkTable() {
        PreparedStatement st = null;
        try {
            st = sqlConnection.prepareStatement("CREATE TABLE IF NOT EXISTS `ultrapvp_players` (" +
                    "`id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                    "`uuid` CHAR(36) NOT NULL UNIQUE, `kills` INTEGER(10) DEFAULT 0, " +
                    "`deaths` INTEGER(10) DEFAULT 0, `lvl` INTEGER(10) DEFAULT 0," +
                    "`points` INTEGER(10) DEFAULT " + ((Config.POINTS_TYPE.equalsIgnoreCase("elo")) ? Config.ELO_START_POINTS : 0) + ", " +
                    "`lvlpoints` INTEGER(10) DEFAULT 0, `lastreward` INTEGER(10) DEFAULT 0, " +
                    "`maxlevel` INTEGER(10) DEFAULT " + UltraPvp.getInstance().getRewardManager().getMaxLvl("default") + ")");
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(st, null);
        }
    }

    public void closeSQLConnection() {
        try {
            if (sqlConnection != null && !sqlConnection.isClosed()) sqlConnection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void checkSQLConnection() {
        if (System.currentTimeMillis() - lastSQLRefresh > 300000) makeGoodSQLConnection();
    }

    public void makeGoodSQLConnection() {
        try {
            if (sqlConnection != null && !sqlConnection.isClosed()) {
                closeSQLConnection();
                openSQLConnection();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            openSQLConnection();
        }
    }

}
