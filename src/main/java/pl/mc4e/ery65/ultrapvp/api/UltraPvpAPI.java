package pl.mc4e.ery65.ultrapvp.api;

import org.bukkit.entity.Player;
import pl.mc4e.ery65.ultrapvp.UltraPvp;
import pl.mc4e.ery65.ultrapvp.managers.*;
import pl.mc4e.ery65.ultrapvp.utils.GamePlayer;

import java.util.UUID;

/**
 * Created by Ery65 on 2016-02-12.
 */
public class UltraPvpAPI {

    public Boolean containsPlayer(UUID uuid) { return UltraPvp.getInstance().getPlayerManager().containsPlayer(uuid); }

    public Boolean containsPlayer(Player player) { return containsPlayer(player.getUniqueId()); }

    public GamePlayer getPlayer(UUID uuid) { return UltraPvp.getInstance().getPlayerManager().getPlayer(uuid); }

    public GamePlayer getPlayer(Player player) { return getPlayer(player.getUniqueId()); }

    @Deprecated
    public Boolean containsPlayer(String name) {
        if (UltraPvp.getInstance().getServer().getPlayer(name) != null) {
            return containsPlayer(UltraPvp.getInstance().getServer().getPlayer(name));
        }
        return Boolean.FALSE;
    }

    @Deprecated
    public GamePlayer getPlayer(String name) {
        if (UltraPvp.getInstance().getServer().getPlayer(name) != null) {
            return getPlayer(UltraPvp.getInstance().getServer().getPlayer(name));
        }
        return null;
    }

    public GradeManager getGradeManager() { return UltraPvp.getInstance().getGradeManager(); }

    public DatabaseManager getDatabaseManager() { return UltraPvp.getInstance().getDatabaseManager(); }

    public PermissionManager getPermissionManager() { return UltraPvp.getInstance().getPermissionManager(); }

    public PlayerManager getPlayerManager() { return UltraPvp.getInstance().getPlayerManager(); }

    public PointsManager getPointsManager() { return UltraPvp.getInstance().getPointsManager(); }

    public RewardManager getRewardManager() { return UltraPvp.getInstance().getRewardManager(); }

}
