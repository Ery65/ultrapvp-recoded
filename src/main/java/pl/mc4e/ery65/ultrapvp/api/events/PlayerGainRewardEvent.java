package pl.mc4e.ery65.ultrapvp.api.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import pl.mc4e.ery65.ultrapvp.UltraPvp;
import pl.mc4e.ery65.ultrapvp.utils.GamePlayer;
import pl.mc4e.ery65.ultrapvp.utils.SimpleReward;

/**
 * Created by Ery65 on 2016-02-12.
 */
public class PlayerGainRewardEvent extends Event implements Cancellable {

    private static final HandlerList handlers = new HandlerList();

    private boolean isCanceled = false, isSpecial = false;

    private GamePlayer gp;

    private SimpleReward reward;

    public PlayerGainRewardEvent(SimpleReward rew, Boolean isSpecial, GamePlayer gamePlayer){
        this.isSpecial = isSpecial;
        reward = rew;
        gp = gamePlayer;
    }

    public void setReward(SimpleReward r){ reward = r; }

    public SimpleReward getReward() { return reward; }

    public GamePlayer getGamePlayer(){ return gp; }

    public boolean isSpecialReward(){ return isSpecial; }

    @Override
    public boolean isCancelled(){
        return isCanceled;
    }

    @Override
    public void setCancelled(boolean toggle){ isCanceled = toggle; }

    @Override
    public HandlerList getHandlers(){ return handlers; }

    public static HandlerList getHandlerList(){ return handlers; }

}
